**fSON_MEG_project**

- Code for analysis performed for "fSON_MEG" project
- Publication: "Sleep-specific processing of auditory stimuli is reflected by alpha and sigma oscillations"
- Analysis can be done on real data from one of the study subjects (MEG + EEG data; sleep stages: W, N1, N2, N3)
- Please note statistics is done here on single-trial data, instead of single-subject data level


**To run the analyses:**
1. Download functions ("main.m" script and "help_functions" folder)
2. Download the sample data from here: https://myfiles.sbg.ac.at/index.php/s/DmspogegyHztpPe (stored on the server of University of Salzburg, Austria)
3. Download obob_ownft toolbox from here: https://gitlab.com/obob/obob_ownft
4. Open "main.m" script to run the code from there
5. Update your paths in the "USER INPUT" section of the "main.m" script (lines: 23-26)
6. Run "add paths" section to set up all the paths
7. You can then run the selected functions independently and in any order
8. If you run "random_trial_selection.m" function, it will affect data used for all of the upcoming analysis
9. You can change main parameters of each function by modifying "USER INPUT" section inside of each function (e.g. select sensors, time window, etc.)
10. You can see the ouput given by each function in the output_figures folder here: https://myfiles.sbg.ac.at/index.php/s/DmspogegyHztpPe

**Software used:**
- Matlab: (https://de.mathworks.com/products/matlab.html)
- obob_ownft toolbox, including Fieldtrip toolbox (https://gitlab.com/obob/obob_ownft)
- Hori staging performed with the toolbox developed by Sridhar Jagannathan: https://github.com/SridharJagannathan/microMeasAlertness_HumanEEG
- Circular statistics performed with the toolbox developed by Philipp Berens & Marc J. Velasco: http://www.kyb.tuebingen.mpg.de/~berens/circStat.html

**Code tested on:**
- Matlab R2019a version 9.6
- Hori Staging toolbox verion 03.03.2018
- Fieldtrip version ea6897bb7
- OBOB release 2.12.0 from 2021-03-31
- Windows 10
