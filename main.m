% ANALYSIS SCRIPTS FOR fSON MEEG PROJECT
%
% -----------------------------------------------------------------
% STUDY INFO
% * ISI types: 2.5:0.5:6 
% * name type: SON (subject own name) & UN (unfamiliar name)
% * voice type: FV (familiar voice) & UFV (unfamiliar voice)
%
% -----------------------------------------------------------------
% SOFTWARE INFO:
% * last update: 2021-08-02
% * tested on: Matlab R2019a version 9.6; Fieldtrip version ea6897bb7
% * OS: Windows 10
% * author: Dr. Malgorzata Wislowska
% * homepage: http://www.sleepscience.at/?page_id=71&lang=en
% * GIT: https://gitlab.com/Wislowska/fson_meg
% -----------------------------------------------------------------

restoredefaultpath; clear all

%% %%%%%%%%%%%%%%%%%%%%%%% USER INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% please update your paths here
dir_data        = ''; % directory to the sample MEEG data
dir_obobownft   = ''; % directory to the folder with OBOB_OWNFT toolbox
dir_scripts     = ''; % directory to the analysis scripts (help functions)
dir_output      = ''; % directory where you want to store the output
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% add paths
% initialize obob_ownft
addpath(dir_obobownft)
cfg             = [];
cfg.package.svs = true;
cfg.package.gm2 = true;
obob_init_ft(cfg); 

% add paths
addpath(genpath(dir_data))
addpath(genpath(dir_scripts))
addpath(dir_output)
cd(dir_output)


%% select random trials (optional - see the function description)
random_trial_selection


%% calculate time-frequncy (TFR) at the sensor level
calculate_induced_responses_sensor


%% plot sensor level power data
% plot evoked responses (ERP/ERF) at the sensor level (Fig 3ab)
plot_evoked_responses_sensor

% plot TFR at the sensor level (Fig 1ab)
plot_induced_responses_sensor

% plot topography of a selected TFR window
plot_induced_responses_topo_sensor

% plot average power spectrum (Fig 1e)
plot_fft_sensor

% plot alignment of narrow- and broad-band ERS  (Fig 3d)
plot_filtered_erps_sensor


%% plot sensor level phase data
% plot inter-trial phace coherence (ITPC) (Fig 4a)
plot_itpc_sensor

% plot instantaneous phase-to-phase coupling (Fig 4b)
plot_phase_angles_sensor


%% statistics on the sensor level data
% pre- to post-stimulus TFR statistical contrast (Fig 1ab)
plot_stat_tfr_sensor_prepost

% contrast between conditions (name and voice effect) (Fig 2ab)
plot_stat_tfr_sensor_conditions


%% source level analysis
% source reconstruction of TLK and TFR data
source_reconstruct_evoked_and_induced_responses


%% plot source level data
% plot evoked responses (ERF) at the source level (Fig 3c)
plot_evoked_responses_source

% source reconstruct specific TFR windows (Fig 1cd)
plot_induced_responses_source

