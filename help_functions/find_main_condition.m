function out = find_main_condition(triggers_name,curr_con)

con_nb  = length(triggers_name);
out     = cell(1,con_nb);

for icon = 1:con_nb
    this_con    = triggers_name{icon};
    both_fac    = strsplit(this_con,'_');
    chosen      = regexp(both_fac,curr_con);
    
    if ~isempty(find([chosen{:}]==1))
        out{icon} = 1;
    end
    
end

end