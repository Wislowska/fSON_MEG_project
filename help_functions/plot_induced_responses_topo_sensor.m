function plot_induced_responses_topo_sensor
% Produces figures stored in:
% output_figures/plot_induced_responses_sensor/xxx

%% %%%%%%%%%%%%%%%%%%%%%%% USER INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set parameters
sens_type       = 'grad';       % {'grad','mag','eeg'} which sensors (gradiometers, magnetometers, eeg)?
bl_corr         = 'yes';        % {'yes','no'} perform baseline correction?
bl_wind         = [-0.5 -0.1];  % [from to] baseline window
my_toi          = [0.1 0.4];    % [from to] time to plot
my_foi          = [3 7];        % [from to] frequency to plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% internal variables
sleep_stages    = {'W','N1','N2','N3'};
ss_nb           = size(sleep_stages,2);
my_cond         = {'SON','UN'}; % two complementary conditions
cond_nb         = size(my_cond,2);


%% select sensor layout template
switch sens_type
    case 'grad'; my_lay = 'neuromag306cmb.lay';
    case 'mag';  my_lay = 'neuromag306mag.lay'; 
    case 'eeg';  my_lay = 'easycapM11';
end


%% loop across sleep stages
for iss = 1:ss_nb
    mss             = sleep_stages{iss}; % my sleep stage
    tfr_all         = cell(0);
    
    
    %% load data
    for icon = 1:cond_nb
        % load tfr data
        curr_con        = my_cond{icon};
        tfr_con         = load([mss,'_',sens_type,'_',curr_con,'_tfr_low']);
        
        % store the data
        tfr_all{icon} 	= tfr_con;
        tfr_con=[]; %clean
    end
    
    
    %% average across conditions
    tfr_app             = ft_appendfreq([],tfr_all{:});
    tfr_app.dimord      = 'rpt_chan_freq_time';
    tfr_all=[]; %clear
    
    
    %% extract oscillatory power
    tfr_app.powspctrm   = abs(tfr_app.fourierspctrm);
    tfr_app             = rmfield(tfr_app,'fourierspctrm');
    
    
    %% combine gradiometers
    if strcmp(sens_type, 'grad')
        cfg            = [];
        cfg.method     = 'sum';
        tfr_cmb        = ft_combineplanar(cfg,tfr_app);
    else
        tfr_cmb        = tfr_app;
    end
    tfr_app=[]; %clear
    
    
    %% baseline correction of single trials
    if strcmp(bl_corr,'yes')
        cfg                 = [];
        cfg.baseline        = bl_wind;
        cfg.baselinetype    = 'absolute';
        cfg.parameter       = 'powspctrm';
        tfr_bl              = ft_freqbaseline(cfg,tfr_cmb);
    else
        tfr_bl              = tfr_cmb;
    end
    tfr_cmb=[]; %clear
    
    
    %% average across trials
    cfg                 = [];
    cfg.avgoverrpt      = 'yes';
    tfr_avg             = ft_selectdata(cfg,tfr_bl);
    ntrl                = size(tfr_bl.trialinfo,1);
    tfr_bl=[]; %clear
       
    
    %% note down toi and foi   
    f1          = num2str(my_foi(1));
    f2          = num2str(my_foi(2));
    t1          = num2str(round(my_toi(1)*1000)); % in ms
    t2          = num2str(round(my_toi(2)*1000)); % in ms
    
    
    %% plot
    my_title        = {['SensType=',sens_type,' SleepStage=',mss],['TrlNb=',num2str(ntrl)],['Foi=[',f1,' ',f2,'] Toi=[',t1,' ',t2,']']};
    cfg             = [];
    cfg.parameter   = 'powspctrm';
    cfg.xlim        = my_toi;
    cfg.ylim        = my_foi;
    cfg.layout      = my_lay;
    cfg.comment     = 'no';
    fh_=figure;ft_topoplotTFR(cfg,tfr_avg);
    
    
    %% make pretty
    title(my_title,'FontSize', 12);
    colorbar
    colormap('jet')

    set(fh_, 'Color', 'w')
    fh_.Children(2).Label.String = 'Relative Change in Oscillatory Power';
    fh_.Children(2).Label.FontSize = 12;
    
    savefig(['TFRtopo_',sens_type,'_',mss,'_Foi-',f1,'-',f2,'Hz_Toi-',t1,'-',t2,'ms'])  
end

end