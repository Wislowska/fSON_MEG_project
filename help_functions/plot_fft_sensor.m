function plot_fft_sensor
% Produces figures stored in:
% output_figures/plot_fft_sensor/xxx

%% %%%%%%%%%%%%%%%%%%%%%%% USER INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set parameters
sens_type       = 'grad';       % {'grad','mag','eeg'} which sensors (gradiometers, magnetometers, eeg)? 
time2plot       = [-2.2 -0.2];  % [from to] time to plot FFT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% internal variables
sleep_stages    = {'W','N1','N2','N3'};
ss_nb           = size(sleep_stages,2);


%% load data
temp_data       = load('data_preproc.mat');
data_roi        = load('data_roi_sens.mat');


%% select roi sensors
roi_sens        = data_roi.(sens_type);


%% decide on yaxis label
switch sens_type
    case 'grad'; ylab = 'Oscillatory Power [T]';
    case 'mag'; ylab = 'Oscillatory Power [T]';
    case 'eeg'; ylab = 'Oscillatory Power [V]';
end


%% loop across sleep stages
for iss = 1:ss_nb
    %% select data for the chosen sleep stage and chosen sensors
    mss             = sleep_stages{iss}; % my sleep stage
    data_sel        = select_sleep_stage(temp_data,mss,sens_type);
    
    
    %% select pre-selected sub-sample of trials
    trls2take       = load(['TrlInfo_',mss,'.mat']);
    selTrls         = trls2take.trlinfo(:,3);
    currTrls        = data_sel.trialinfo(:,3);
    trls2take       = ismember(currTrls,selTrls);
    
    cfg             = [];
    cfg.trials      = trls2take;
    curr_data       = ft_selectdata(cfg,data_sel);
    data_sel=[]; %clear

    
    %% select time window of interest
    cfg             = [];
    cfg.latency     = time2plot;
    data_time       = ft_selectdata(cfg,curr_data);
    curr_data=[]; %clear
    
    
    %% demean data
    cfg                 = [];
    cfg.demean          = 'yes';
    cfg.baselinewindow  = 'all';
    data_detr           = ft_preprocessing(cfg, data_time);
    data_detr           = rmfield(data_detr,'sampleinfo');
    time_line           = data_detr.time{1};
    data_time=[]; %clear
    
    
    %% calculate FFT
    cfg                 = [];
    cfg.foi             = 1:1:30;
    cfg.keeptrials      = 'yes';
    cfg.output          = 'pow';
    cfg.method          = 'mtmfft';
    cfg.taper           = 'hanning';
    data_fr             = ft_freqanalysis(cfg, data_detr);
    data_detr=[]; %clear
    
    
    %% combine gradiometers
    if strcmp(sens_type, 'grad') % gradiometeres
        cfg         = [];
        cfg.method  = 'sum';
        data_cmb  	= ft_combineplanar(cfg,data_fr);
    else % magnetometers
        data_cmb    = data_fr;
    end
    data_fr=[]; %clear

    
    %% average across trials
    cfg            	= [];
    cfg.avgoverrpt 	= 'yes';
    data_avg        = ft_selectdata(cfg,data_cmb);
    ntrl            = size(data_cmb.trialinfo,1);
    data_cmb=[]; %clear
    
    
    %% select ROI sensors
    cfg                 = [];
    cfg.channel         = roi_sens.cmb;
    data_avg_roi        = ft_selectdata(cfg, data_avg);

    
    %% average across sensors
    cfg             = [];
    cfg.avgoverchan = 'yes';
    data_fin        = ft_selectdata(cfg,data_avg);
    data_fin_10     = ft_selectdata(cfg,data_avg_roi);
    data_avg=[];data_avg_roi=[]; %clear
    
    
    %% plot all sensors
    fh1_ = figure; plot(data_fin.freq,data_fin.powspctrm)
    ca   = gca;
    
    % describe 
    title({['SensNb=all SensType=',sens_type],['SleepStage=',mss],['TrlNb=',num2str(ntrl)]});
    xlabel('Frequency [Hz]');
    ylabel(ylab);
    
    % make pretty
    ca.FontSize             = 12;
    ca.Children.LineWidth   = 2;
    ca.Children.Color       = 'k';
    ca.XLim                 = [1 30];
    set(fh1_, 'Color', 'w')
    savefig(['FFT_',sens_type,'_',mss,'_AllSens'])

  
    %% plot ROI sensors
    fh2_ = figure; plot(data_fin_10.freq,data_fin_10.powspctrm)
    ca   = gca;
    
    % describe
    title({['SensNb=roi SensType=',sens_type],['SleepStage=',mss],['TrlNb=',num2str(ntrl)]});
    xlabel('Frequency [Hz]');
    ylabel(ylab);
    
    % make pretty
    ca.FontSize             = 12;
    ca.Children.LineWidth   = 2;
    ca.Children.Color       = 'k';
    ca.XLim                 = [1 30];
    set(fh2_, 'Color', 'w')
    savefig(['FFT_',sens_type,'_',mss,'_Roi'])
    
    data_fin=[]; data_fin_10=[]; %clear
end
end