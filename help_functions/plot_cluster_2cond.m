function plot_cluster_2cond(stat,ctype,data_in,mss,contrast)
%% interpolate data
tfr_avg             = squeeze(nanmean(data_in.powspctrm,1));
[data_int,tim_int,fre_int,my_lim] = interpolate_data(tfr_avg,data_in.time,data_in.freq);
tfr_avg=[]; %clear


%% create a mask
my_voxels       = stat.([ctype,'clusterslabelmat'])==1;

% create a mask parameter
mymask          = squeeze(mean(my_voxels, 1));
chan_nb         = size(my_voxels,1);
temp            = repmat(logical(mymask),1,1,chan_nb);
final_mask      = shiftdim(temp,2);

% interpolate the mast to plot nicely
mask_avg        = squeeze(nanmean(final_mask,1));
[mask_int]      = interpolate_data(mask_avg,data_in.time,data_in.freq);
mask_avg=[]; %clear


%% plot interpolated
fh1_=figure;
imagesc(tim_int, fre_int, data_int)
hcb=colorbar;
hold on

% add contour
contour(tim_int,fre_int,mask_int,1,'LineColor',[0 0 0],'LineWidth',2)
caxis([-my_lim my_lim])
colormap('jet')
axis xy

% add description
hcb.Label.String  = 'Relative Change in Oscillatory Power';
xlabel('Time around stimulus onset [sec.]')
ylabel('Frequency [Hz.]')
p_val      = stat.([ctype,'clusters'])(1).prob;
title({['(',contrast,') ',mss],['clus p-val=',num2str(p_val)]})

% make pretty
set(gca,'FontSize',14);
set(gcf, 'Color', 'w')

myylim      = get(gca,'YLim');
ylevel      = myylim(2)-myylim(2)/10;
line([0 0.725],[ylevel ylevel], 'Color', [.3 .3 .3], 'LineStyle','-', 'LineWidth', 2, 'Marker','.','MarkerSize',14) %
text(0,ylevel,'  avg. stim. duration','Color',[.3 .3 .3],'FontAngle','italic','HorizontalAlignment','left','VerticalAlignment','bottom','FontSize',11)
line([0 0],myylim, 'Color', [.1 .1 .1], 'LineStyle','--', 'LineWidth', 2) 

end