function data_out = select_sleep_stage(data_in,sleep_stage,sens_type)
%% find trials only from the wake part
log_file        = importdata('log_wake.txt',' ');
nb_of_w_trl     = size(log_file.data,1);

% find the last wake trial in the current MEG data
curr_data       = data_in.trialinfo;
last_W_trial    = find_wake(curr_data,nb_of_w_trl);


%% if wake, select trials only from the wake part
if strcmp(sleep_stage,'W') % wake
    cfg             = [];
    cfg.trials      = 1:last_W_trial;
    data_sel        = ft_selectdata(cfg,data_in);
else % sleep
    cfg             = [];
    cfg.trials      = last_W_trial+1:size(data_in.trial,2);
    data_sel        = ft_selectdata(cfg,data_in); 
end
data_in=[]; %clear


%% select sleep stage
ss_nb           = recode_sleep_stage(sleep_stage);
if ~isempty(data_sel.trial)
    cfg             = [];
    cfg.trials      = data_sel.trialinfo(:,2)==ss_nb;
    data_stage      = ft_selectdata(cfg,data_sel);
else
    data_stage      = data_sel;
end
data_sel=[]; %clear


%% make sure you have good number of channels
data_out   = fix_my_channels(data_stage,1,sens_type);

end