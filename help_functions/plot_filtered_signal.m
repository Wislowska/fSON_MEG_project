function plot_filtered_signal(filt_sig,avg_all,freq_boarders,my_time,save_name)
%% intrinsic variables
band_nb     = size(freq_boarders,1);
pl_nb       = floor((band_nb+1)/2)+1;


%% find ylims
my_min      = min(min(filt_sig));
my_max      = max(max(filt_sig));


%% plot filteres signal
fh_=figure; %('Units','normalized','Position',[0 0 1 1]);
subplot(pl_nb,2,1)
plot(my_time,avg_all,'Color','k')
ylabel('freq: all')
line([0 0],ylim, 'Color', [.1 .1 .1], 'LineStyle',':', 'LineWidth', 1) % predicted

for ib = 1:band_nb
    subplot(pl_nb,2,ib+1)
    plot(my_time,filt_sig(ib,:),'Color','k');
    ylabel(['freq: ',num2str(freq_boarders(ib,1)),'-',num2str(freq_boarders(ib,2))])
    line([0 0],ylim, 'Color', [.1 .1 .1], 'LineStyle',':', 'LineWidth', 1) % predicted
end


%% save figure
savefig(fh_,save_name)
end