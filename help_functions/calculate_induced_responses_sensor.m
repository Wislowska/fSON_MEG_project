function calculate_induced_responses_sensor
% Produces data stored in:
% sample_data/tfr/xxx


%% %%%%%%%%%%%%%%%%%%%%%%% USER INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set parameters
sens_type       = 'mag';       % {'grad','mag','eeg'} which sensors (gradiometers, magnetometers, eeg)?
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% internal variables
my_cond         = {'FV','SON','UFV','UN'};
cond_nb         = size(my_cond,2);
sleep_stages    = {'W','N1','N2','N3'};
ss_nb           = size(sleep_stages,2);


%% load data
temp_data       = load('data_preproc.mat');


%% loop across sleep stages
for iss = 1:ss_nb
    %% select data for the chosen sleep stage and chosen sensors
    mss             = sleep_stages{iss}; % my sleep stage
    data_sel        = select_sleep_stage(temp_data,mss,sens_type);
    
    
    %% loop through the conditions
    for icon = 1:cond_nb
        %% select condition trials
        curr_con            = my_cond{icon};
        meg_curr            = select_condition_trials(data_sel,mss,curr_con);
        
        
        %% demean data
        cfg                 = [];
        cfg.demean          = 'yes';
        cfg.baselinewindow  = 'all';
        meg_detr            = ft_preprocessing(cfg, meg_curr);
        meg_detr            = rmfield(meg_detr,'sampleinfo');
        time_line           = meg_detr.time{1};
        meg_curr=[];
        
        
        %% low frequencies
        cfg                 = [];
        cfg.toi             = time_line(1):0.05:time_line(end); % where is center for the window
        cfg.foi             = 1:1:30;
        cfg.keeptrials      = 'yes';
        cfg.output          = 'fourier';
        cfg.method          = 'mtmconvol';
        cfg.t_ftimwin       = 3./cfg.foi; % variable time window
        cfg.t_ftimwin(1)    = 1; % 1Hz
        cfg.t_ftimwin(2)    = 1; % 2Hz
        cfg.taper           = 'hanning';
        tfr_low             = ft_freqanalysis(cfg, meg_detr);
        save([mss,'_',sens_type,'_',curr_con,'_tfr_low'],'-struct','tfr_low')
      
        
        %% high frequencies
% % %         cfg                 = [];
% % %         cfg.toi             = time_line(1):0.05:time_line(end); % where is center for the window
% % %         cfg.foi             = 30:3:120;
% % %         cfg.keeptrials      = 'yes';
% % %         cfg.output          = 'fourier';
% % %         cfg.method          = 'mtmconvol';
% % %         cfg.t_ftimwin       = ones(length(cfg.foi),1).* 0.25;
% % %         cfg.taper           = 'dpss';
% % %         cfg.tapsmofrq       = 10;
% % %         tfr_high            = ft_freqanalysis(cfg, meg_detr);
% % %         save([mss,'_',sens_type,'_',curr_con,'_tfr_high'],'tfr_high')        

    end   
end
end