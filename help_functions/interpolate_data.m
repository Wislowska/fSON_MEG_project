 function [data_int,tim_int,fre_int,my_lim] = interpolate_data(powspctrm,time,freq)
 
% interpolate data to plot nicely
avg2plot  	= squeeze(powspctrm);
my_lim     	= max(abs(avg2plot(:)));

% make grid for fine plotting
tim_int   	= linspace(time(1), time(end), 512);
fre_int   	= linspace(freq(1), freq(end), 512);
[tim_grid_orig, freq_grid_orig]         = meshgrid(time, freq);
[tim_grid_interp, freq_grid_interp]     = meshgrid(tim_int, fre_int);

% interpolate
data_int     = interp2(tim_grid_orig, freq_grid_orig, avg2plot,...
    tim_grid_interp, freq_grid_interp, 'spline');

 end