function find_erp_peaks(avg_ft,freq_boarders,filt_sig,save_name)
%% intrinssic variables
band_nb     = size(freq_boarders,1);
my_time     = avg_ft.time;
pl_nb       = floor((band_nb+1)/2);


%% find peaks of erp
[~,aa]      = findpeaks(avg_ft.avg);
[~,bb]      = findpeaks(-avg_ft.avg);

% convert signal
sigl                = size(avg_ft.avg,2);
erp_signal          = zeros(1,sigl);
aaa                 = sort([aa,aa+1,aa-1,aa+2,aa-2]);
bbb                 = sort([bb,bb+1,bb-1,bb+2,bb-2]);
aaa(aaa==0)         = [];
bbb(bbb==0)         = [];
erp_signal(aaa)     = 1;
erp_signal(bbb)     = -1;
erp_signal(sigl+1:end) = [];


%% find peaks of the filtered signal
for ib = 1:band_nb
    cfreq       = freq_boarders(ib,:);
    sig2take    = filt_sig(ib,:);
    sig2take2   = -sig2take;
    [~,pp]      = findpeaks(sig2take);
    [~,dd]      = findpeaks(sig2take2);
    
    % convert signal
    sigl                = size(sig2take,2);
    fin_signal          = zeros(1,sigl);
    ppp                 = sort([pp,pp+1,pp-1,pp+2,pp-2,pp+3,pp-3]);
    ddd                 = sort([dd,dd+1,dd-1,dd+2,dd-2,dd+3,dd-3]);
    ppp(ppp<=0)         = [];
    ddd(ddd<=0)         = [];
    fin_signal(ppp)     = 1;
    fin_signal(ddd)     = -1;
    fin_signal(sigl+1:end) = [];
       
    % multiply the converted signal with the erp
    corr                = erp_signal .* fin_signal;
    
    % find positive correlation (in phase)
    time_corr           = my_time(corr==1);
    agreed              = size(time_corr,2);
    
    % plot the converted signal
    fh_=figure;
    subplot(pl_nb,2,1)
    plot(my_time,avg_ft.avg,'Color','k')
    ylabel('freq: all')
    for ic = 1:agreed
        cc = time_corr(ic);
        line([cc cc],ylim, 'Color', 'r', 'LineStyle',':', 'LineWidth', 0.5)
    end
    line([0 0],ylim, 'Color', [.1 .1 .1], 'LineStyle','--', 'LineWidth', 1)
    
    subplot(pl_nb,2,2)
    plot(my_time,sig2take,'Color','k')
    ylabel(['freq: ',num2str(cfreq(1)),' to ',num2str(cfreq(2))])
    for ic = 1:agreed
        cc = time_corr(ic);
        line([cc cc],ylim, 'Color', 'r', 'LineStyle',':', 'LineWidth', 0.5)
    end
    line([0 0],ylim, 'Color', [.1 .1 .1], 'LineStyle','--', 'LineWidth', 1)
    
    subplot(pl_nb, 2,3)
    plot(my_time,avg_ft.avg,'Color','k')
    hold on
    plot(my_time(aa),avg_ft.avg(aa),'bo')
    plot(my_time(bb),avg_ft.avg(bb),'bo')
    ylabel('freq: all')
    line([0 0],ylim, 'Color', [.1 .1 .1], 'LineStyle','--', 'LineWidth', 1)
    
    subplot(pl_nb,2,4)
    plot(my_time,sig2take,'Color','k')
    hold on
    plot(my_time(pp),sig2take(pp),'bo')
    plot(my_time(dd),sig2take(dd),'bo')
    ylabel(['freq: ',num2str(cfreq(1)),' to ',num2str(cfreq(2))])
    line([0 0],ylim, 'Color', [.1 .1 .1], 'LineStyle','--', 'LineWidth', 1)
    
    subplot(pl_nb,2,5)
    plot(my_time,fin_signal,'Color','k')
    ylabel(['freq: ',num2str(cfreq(1)),' to ',num2str(cfreq(2))])
    line([0 0],ylim, 'Color', [.1 .1 .1], 'LineStyle','--', 'LineWidth', 1)
    
    subplot(pl_nb,2,6)
    plot(my_time,erp_signal,'Color','k')
    ylabel('freq: all')
    line([0 0],ylim, 'Color', [.1 .1 .1], 'LineStyle','--', 'LineWidth', 1)
    
    subplot(pl_nb,2,7)
    plot(my_time,corr,'Color','k')
    ylabel('correlation')
    line([0 0],ylim, 'Color', [.1 .1 .1], 'LineStyle','--', 'LineWidth', 1)
    
    
    %% save figure
    curr_name       = [save_name,num2str(cfreq(1)),'_',num2str(cfreq(2)),'Hz'];
    savefig(fh_,curr_name)
    close(fh_)
end
end