function triggers = extract_cond_trig(array,out)

temp    = array/10;
first   = floor(temp);
second  = round((temp - first)*10);

switch out
    case 1; triggers = first;
    case 2; triggers = second;
end

end