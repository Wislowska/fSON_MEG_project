function plot_evoked_responses_source
% Produces figures stored in:
% output_figures/plot_evoked_responses_source

%% %%%%%%%%%%%%%%%%%%%%%%% USER INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set parameters
bl_wind         = [-0.5 -0.1];  % [from to] baseline window 
time2plot       = [0.1 0.3];   % [from to] time to plot ERP/ERF
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% internal variables
sleep_stages    = {'W','N1','N2','N3'};
ss_nb           = size(sleep_stages,2);


%% load data
% load template grid
temp_grid    	= load('mni_grid_1_5_cm_889pnts');
temp_grid     	= temp_grid.template_grid;
temp_grid      	= ft_convert_units(temp_grid, 'm');

% load template mri
temp_mri       	= ft_read_mri('single_subj_T1.nii');
temp_mri        = ft_convert_units(temp_mri,'m');
temp_mri        = ft_volumereslice([],temp_mri);
temp_seg_mri    = load('template_seg_mri');


%% loop across sleep stages
for iss=1:ss_nb
    mss             = sleep_stages{iss};
    curr_data       = load(['tlk_source_',mss]);
    
    %% baseline correct
    cfg                 = [];
    cfg.baseline        = bl_wind;
    cfg.baselinetype    = 'relchange';
    data_bl             = obob_svs_timelockbaseline(cfg,curr_data);
    curr_data=[];
    
    
    %% bring to source space of a template brain
    cfg                 = [];
    cfg.sourcegrid      = temp_grid; %temp_grid
    cfg.parameter       = 'avg';
    cfg.mri             = temp_mri; %temp_mri
    cfg.latency         = time2plot;
    source              = obob_svs_virtualsens2source(cfg, data_bl);
    data_bl=[]; %clear
    
    
    %% threshold at half-maximum
    thres               = 0.5;
    source.mask         = source.avg >= (max(source.avg(:)) * thres);
    
    
    %% mask out everything else than brain
    source.mask         = source.mask .* temp_seg_mri.brain;
    
    
    %% plot ortho
    cfg                 = [];
    cfg.method          = 'ortho';
    cfg.funparameter    = 'avg';
    cfg.funcolormap     = 'parula';
    cfg.maskparameter   = 'mask';
    cfg.interactive     = 'yes';
    cfg.funcolorlim     = 'zeromax';
    cfg.crosshair       = 'no';
    ft_sourceplot(cfg, source);
    fh_ = gcf;
    d = jet(256);
    colormap(d(256/2:end,:));
    
    
    %% save plot
    savefig(fh_,['ERF_',mss])
    source=[]; %clear
end