function plot_phase_angles_sensor
% Produces figures stored in:
% output_figures/plot_phase_alignment_sensor/xxx

%% %%%%%%%%%%%%%%%%%%%%%%% USER INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set parameters
sens_type       = 'grad';       % {'grad','mag','eeg'} which sensors (gradiometers, magnetometers, eeg)? 
time2plot       = [-0.5 1.5];   % [from to] time to plot ERP/ERF
CI              = 0.05;         % confidence interval for omnibus test
alpha           = 0.01;         % alpha level for omnibus test
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% internal variables
sleep_stages    = {'W','N1','N2','N3'};
ss_nb           = size(sleep_stages,2);
my_cond         = {'SON','UN'}; % two complementary conditions
cond_nb         = size(my_cond,2);


%% select roi sensors
data_roi        = load('data_roi_sens.mat');
roi_sens        = data_roi.(sens_type);


%% load data
for iss = 1:ss_nb
    mss             = sleep_stages{iss}; % my sleep stage
    tfr_all         = cell(0);
    
    %% load data
    for icon = 1:cond_nb
        % load tfr data
        curr_con        = my_cond{icon};
        tfr_con         = load([mss,'_',sens_type,'_',curr_con,'_tfr_low']);
        
        % store the data
        tfr_all{icon} 	= tfr_con;
        tfr_con=[]; %clean
    end
    
    
    %% combine conditions
    tfr_app             = ft_appendfreq([],tfr_all{:});
    tfr_app.dimord      = 'rpt_chan_freq_time';
    tfr_all=[]; %clear
    
    
    %% select shorter data
    cfg             = [];
    cfg.latency     = time2plot;
    data_sel        = ft_selectdata(cfg,tfr_app);
    tfr_app=[];
    
    
    %% extract phase info
    data_sel.fourierspctrm = angle(data_sel.fourierspctrm);
    
    
    %% Hodges-Ajne test (omnibus test) for nonuniformity
    data_out        = run_hodges_ajne_test(data_sel,CI,alpha);
    
    
    %% for each time-frequency point and each channel, find main direction over trials
    chan_nb         = size(data_sel.label,1);
    time_nb         = size(data_sel.time,2);
    freq_nb         = size(data_sel.freq,2);
    
    data_avg        = nan([chan_nb, freq_nb,time_nb]); % chan x freq x time
    
    for ichan = 1:chan_nb
        for ifreq = 1:freq_nb
            for itime = 1:time_nb
                curr_data   = data_out(:,ichan,ifreq,itime);
                data2take   = curr_data(~isnan(curr_data));
                % figure;circ_plot(data2take)
                mean_dir    = circ_median(data2take);
                data_avg(ichan,ifreq,itime) = mean_dir;
            end
        end
    end
    ntrl    = size(data_out,1);
    data_out=[]; %clear
    
    
    %% find ROI channels
    [~,chan2take]   = intersect(data_sel.label,roi_sens.all);
    
    
    %% for each time-frequency point and each channel, find main direction over channels
    data_fin        = nan([freq_nb,time_nb]); % freq x time
    data_fin_roi    = nan([freq_nb,time_nb]); % freq x time
    
    for ifreq = 1:freq_nb
        for itime = 1:time_nb
            curr_data   = data_avg(:,ifreq,itime);
            data2take   = curr_data(~isnan(curr_data));
            % figure;circ_plot(data2take)
            data_fin(ifreq,itime)       = circ_median(data2take);
            data_fin_roi(ifreq,itime)   = circ_median(data2take(chan2take));
        end
    end
    data_avg=[]; %clear
    
    
    %% plot all channels
    data2plot   = -abs(rad2deg(data_fin));
    
    fh_=figure; pcolor(data_sel.time, data_sel.freq, data2plot);
    shading flat
    caxis([-180 0])
    colorbar('Ticks',[-180 0],'TickLabels',{'trough','peak'})
    xlabel('Time around stimulus onset [sec.]')
    ylabel('Frequency [Hz.]')
    title({['SensNb=All SensType=',sens_type],['SleepStage=',mss],['TrlNb=',num2str(ntrl)]})
    line([0 0],[data_sel.freq(1) data_sel.freq(end)], 'Color', [.1 .1 .1], 'LineStyle',':', 'LineWidth', 3)
    cl = get(gca,'XLim');
    set(gca,'XLim',[cl(1)-0.01 cl(2)+0.01]);
    set(fh_, 'Color', 'w')
    set(gca,'FontSize',12)

    % save plot
    savefig(['angles_',sens_type,'_',mss])
    
    
    %% plot ROI
    data2plot   = -abs(rad2deg(data_fin_roi));
    
    fh_=figure; pcolor(data_sel.time, data_sel.freq, data2plot);
    shading flat
    caxis([-180 0])
    colorbar('Ticks',[-180 0],'TickLabels',{'trough','peak'})
    xlabel('Time around stimulus onset [sec.]')
    ylabel('Frequency [Hz.]')
    title({['SensNb=Roi SensType=',sens_type],['SleepStage=',mss],['TrlNb=',num2str(ntrl)]})
    line([0 0],[data_sel.freq(1) data_sel.freq(end)], 'Color', [.1 .1 .1], 'LineStyle',':', 'LineWidth', 3)
    cl = get(gca,'XLim');
    set(gca,'XLim',[cl(1)-0.01 cl(2)+0.01]);
    set(fh_, 'Color', 'w')
    set(gca,'FontSize',12)

    % save plot
    savefig(['angles_',sens_type,'_',mss,'_ROI'])
    
    data_fin=[];data_fin_roi=[]; %clear
end
end