% temp_data       = load('data_preproc.mat');
sleep_stages    = {'W','N1','N2','N3'};
log_file        = importdata('log_wake.txt',' ');
hori_data       = importdata('hori_trls.mat');
triggers        = load('data_triggers.mat');
sens_type       = 'grad';
my_cond         = {'SON_FV','SON_UFV','UN_FV','UN_UFV'};
cond_nb         = size(my_cond,2);

for iss = 1:4
    mss             = sleep_stages{iss};
    data_sel        = select_sleep_stage(temp_data,mss,log_file,sens_type);
    
    trial_data      = data_sel.trialinfo(:,3);
    trl2take        = ~ismember(trial_data,hori_data);
    
    cfg             = [];
    cfg.trials      = trl2take;
    data_arousal    = ft_selectdata(cfg,data_sel);
    
    trl_structure   = data_arousal.trialinfo;
    con_trig        = extract_cond_trig(trl_structure(:,1),2);
    
    for icon = 1:cond_nb
        curr_con        = my_cond{icon};
        
        %% find triggers
        con2take        = contains(triggers.name,curr_con);
        my_trig         = triggers.nb(con2take);
        
        
        %% find trials
        trls2take       = ismember(con_trig,my_trig);
        trls_cond       = trl_structure(trls2take,:);
        size(trls_cond,1)
    end
end