function data_out = select_condition_trials(data_in,mss,curr_con)

% load trials that should be included
trl2use         = load(['TrlInfo_',mss,'_',curr_con]);
take_trls       = trl2use.trl_info(:,3);

% find these trials in the trial info
all_trls        = data_in.trialinfo(:,3);
my_trls         = ismember(all_trls,take_trls);

% find trials
cfg             = [];
cfg.trials      = my_trls;
data_out        = ft_selectdata(cfg,data_in);

end
