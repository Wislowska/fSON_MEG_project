function plot_cluster(stat,ctype,data_in,time_pre,time_post,mss)
%% prepare data for plotting
% baseline correction of single trials
cfg                 = [];
cfg.baseline        = time_pre;
cfg.baselinetype    = 'absolute';
cfg.parameter       = 'powspctrm';
data_bl             = ft_freqbaseline(cfg,data_in);
data_in=[]; %clear

% average across trials
cfg                 = [];
cfg.avgoverrpt      = 'yes';
data_bl_avg         = ft_selectdata(cfg,data_bl);
data_bl=[]; %clear

% select shorter time windows
cfg                 = [];
cfg.latency         = time_post;
data_plot           = ft_selectdata(cfg,data_bl_avg);
data_bl_avg=[];


%% interpolate data
tfr_avg             = squeeze(nanmean(data_plot.powspctrm,1));
[data_int,tim_int,fre_int,my_lim] = interpolate_data(tfr_avg,data_plot.time,data_plot.freq);
tfr_avg=[]; %clear


%% create a mask
my_voxels       = stat.([ctype,'clusterslabelmat'])==1;

% create a mask parameter
mymask          = squeeze(mean(my_voxels, 1));
chan_nb         = size(my_voxels,1);
temp            = repmat(logical(mymask),1,1,chan_nb);
final_mask      = shiftdim(temp,2);

% interpolate the mast to plot nicely
mask_avg        = squeeze(nanmean(final_mask,1));
[mask_int]      = interpolate_data(mask_avg,data_plot.time,data_plot.freq);
mask_avg=[]; %clear


%% plot interpolated
fh1_=figure;
imagesc(tim_int, fre_int, data_int)
hcb=colorbar;
hold on

% add contour
contour(tim_int,fre_int,mask_int,1,'LineColor',[0 0 0],'LineWidth',2)
caxis([-my_lim my_lim])
colormap('jet')
axis xy

% add description
hcb.Label.String  = 'Relative Change in Oscillatory Power';
xlabel('Time around stimulus onset [sec.]')
ylabel('Frequency [Hz.]')
p_val      = stat.([ctype,'clusters'])(1).prob;
title({['(POST - PRE) ',mss],['clus p-val=',num2str(p_val)]})

% make pretty
set(gca,'FontSize',14);
set(gcf, 'Color', 'w')

myylim      = get(gca,'YLim');
ylevel      = myylim(2)-myylim(2)/10;
line([0 0.725],[ylevel ylevel], 'Color', [.3 .3 .3], 'LineStyle','-', 'LineWidth', 2, 'Marker','.','MarkerSize',14) %
text(0,ylevel,'  avg. stim. duration','Color',[.3 .3 .3],'FontAngle','italic','HorizontalAlignment','left','VerticalAlignment','bottom','FontSize',11)
end