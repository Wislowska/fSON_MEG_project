function source_reconstruct_evoked_and_induced_responses
% Produces data stored in /sample_data/source
% Head model for this subject was created based on template MNI brain scan

%% %%%%%%%%%%%%%%%%%%%%%%% USER INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set parameters
bl_wind         = [-0.5 -0.1];  % [from to] baseline window
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% internal variables
my_cond         = {'FV','SON','UFV','UN'};
cond_nb         = size(my_cond,2);
sleep_stages    = {'W','N1','N2','N3'};
ss_nb           = size(sleep_stages,2);


%% load data
temp_data       = load('data_preproc.mat');
log_file        = importdata('log_wake.txt',' ');
raw_fn          = load('raw_filenames.mat');


%% file names of the raw data
raw_fn          = raw_fn.raw_filenames;
fil_nb          = length(raw_fn);


%% loop across sleep stages
for iss = 1:ss_nb
    %% select specific sleep stage and sensor data with interpolation
    mss             = sleep_stages{iss}; % my sleep stage
    data_sel        = select_sleep_stage(temp_data,mss,log_file,'meg');
    
    
    %% select pre-selected sub-sample of trials
    trls2take       = load(['TrlInfo_',mss,'.mat']);
    selTrls         = trls2take.trlinfo(:,3);
    currTrls        = data_sel.trialinfo(:,3);
    trls2take       = ismember(currTrls,selTrls);
    
    cfg             = [];
    cfg.trials      = trls2take;
    curr_data       = ft_selectdata(cfg,data_sel);
    
    
    %% pre-allocate output structure
    all_source      = cell(fil_nb,1);
    
    
    %% baseline correction to get rid of DC offset
    cfg                 = [];
    cfg.demean          = 'yes';
    cfg.baselinewindow  = bl_wind;
    data_bl             = ft_preprocessing(cfg, curr_data);
    curr_data=[]; %clear
    
    
    %% see to which raw file belong these trials
    file_trls       = data_bl.trialinfo(:,4);
    
    
    %% loop across recording files
    for ifil=1:fil_nb
        curr_fil        = raw_fn{ifil};
        
        %% select data
        trl2take        = file_trls==ifil;
        cfg             = [];
        cfg.trials      = trl2take;
        data_sel        = ft_selectdata(cfg,data_bl);
        
        if ~isempty(data_sel.trial)
            %% put original grad definition
            my_hdr             = load(['hdr_',curr_fil]);
            
            % match gradiometers and magnetometers units
            my_hdr.grad    	= ft_convert_units(my_hdr.grad,'m');
            data_sel.hdr       = my_hdr;
            data_sel.grad      = my_hdr.grad;
            
            
            %% load head model
            hdm                = load([curr_fil,'_hdm']);
            warped_grid        = load([curr_fil,'_warped_grid']);
            
            
            %% compute leadfield
            cfg                 = [];
            cfg.sourcemodel     = warped_grid; % individual (warped) grid
            cfg.headmodel       = hdm; % individual headmodel
            cfg.normalize       = 'no';
            curr_lf             = ft_prepare_leadfield(cfg, data_sel);
            
            
            %% compute spatial filters
            % low-pass filter data for filters
            cfg                 = [];
            cfg.lpfilter        = 'yes';
            cfg.lpfreq          = 40;
            data_sel_lp         = ft_preprocessing(cfg,data_sel);
            
            % compute tlk for for spatial filters
            cfg                 = [];
            cfg.covariance      = 'yes';
            data_tlk            = ft_timelockanalysis(cfg,data_sel_lp);
            data_sel_lp=[]; %clear
            
            % compute spatial filters
            cfg                 = [];
            cfg.grid            = curr_lf;
            cfg.fixedori        = 'yes';
            cfg.regfac          = '5%';
            filt                = obob_svs_compute_spat_filters(cfg,data_tlk);
            data_tlk=[];
            
            
            %% source reconstruction
            cfg                 = [];
            cfg.spatial_filter  = filt;
            source              = obob_svs_beamtrials_lcmv(cfg,data_sel);
            
            
            %% store data for later concatination
            all_source{ifil}    = source;
            source=[];
        end
    end
    
    %% remove empty files and append
    data_all        = all_source(~cellfun(@isempty,all_source));
    data_app        = ft_appenddata([],data_all{:});
    all_source=[];data_all=[];
    
    
    %% do tlk in source space
    % low-pass filter source data
    cfg                 = [];
    cfg.lpfilter        = 'yes';
    cfg.lpfreq          = 40;
    source_lp           = ft_preprocessing(cfg,data_app);
    
    % do tlk
    source_tlk          = ft_timelockanalysis([],source_lp);
    source_lp=[];
    
    % save tlk source data on hd
    save(['tlk_source_',mss],'-struct','source_tlk');
    source_tlk=[];
    
    
    %% do tfr in source space
    time_line           = data_app.time{1};
    
    % tfr analysis
    cfg                 = [];
    cfg.toi             = time_line(1) : 0.05 : time_line(end);
    cfg.foi             = 1:1:30;
    cfg.keeptrials      = 'yes';
    cfg.output          = 'pow';
    cfg.method          = 'mtmconvol';
    cfg.t_ftimwin       = 3./cfg.foi;
    cfg.t_ftimwin(1)    = 1; % 1Hz
    cfg.t_ftimwin(2)    = 1; % 2Hz
    cfg.taper           = 'hanning';
    source_tfr          = ft_freqanalysis(cfg, data_app);
    
    % baseline correct
    cfg                 = [];
    cfg.baseline        = bl_wind;
    cfg.baselinetype    = 'relchange';
    cfg.parameter       = 'powspctrm';
    source_tfr_bl       = ft_freqbaseline(cfg,source_tfr);
    
    % average across trials
    cfg                 = [];
    cfg.avgoverrpt      = 'yes';
    source_tfr_avg    	= ft_selectdata(cfg,source_tfr);
    source_tfr_avg_bl  	= ft_selectdata(cfg,source_tfr_bl);
    source_tfr_bl=[];
    
    % save tfr source data on hd
    save(['tfr_source_',mss],'-struct','source_tfr_avg');
% % %     save(['tfr_source_',mss,'_bl'],'-struct','source_tfr_avg_bl');
    
    
    %% separate conditions
    for icon = 1:cond_nb
        % select condition trials
        curr_con            = my_cond{icon};
        meg_curr            = select_condition_trials(source_tfr,mss,curr_con);
        
        % baseline correct
        cfg                 = [];
        cfg.baseline        = bl_wind;
        cfg.baselinetype    = 'relchange';
        cfg.parameter       = 'powspctrm';
        meg_curr_bl         = ft_freqbaseline(cfg,meg_curr);
        
        % average across trials
        cfg                 = [];
        cfg.avgoverrpt      = 'yes';
        source_tfr_avg    	= ft_selectdata(cfg,meg_curr);
        source_tfr_avg_bl  	= ft_selectdata(cfg,meg_curr_bl);
        meg_curr=[];meg_curr_bl=[];
        
        % save tfr source data on hd
        save(['tfr_source_',mss,'_',curr_con],'-struct','source_tfr_avg');
% % %         save(['tfr_source_',mss,'_',curr_con,'_bl'],'-struct','source_tfr_avg_bl');
        source_tfr_avg=[];source_tfr_avg_bl=[];
    end
end
end