function data_out = run_hodges_ajne_test(data_in,CI,alpha)

chan_nb         = size(data_in.label,1);
time_nb         = size(data_in.time,2);
trl_nb          = size(data_in.fourierspctrm,1);
data_out        = nan(size(data_in.fourierspctrm));

for itrl = 1:trl_nb
    for ichan = 1:chan_nb
        for itime = 1:time_nb
            curr_data       = squeeze(data_in.fourierspctrm(itrl,ichan,:,itime));
            pval            = circ_otest(curr_data);
            % figure;circ_plot(curr_data)
            
            if pval<alpha % alpha level
                
                % find data within xx% CI
                me      = circ_mean(curr_data);
                ci      = circ_confmean(curr_data, CI); % CI
                
                if ~isnan(ci)
                    con1        = curr_data>(me-ci);
                    con2        = curr_data<(me+ci);
                    idx2take    = find(con1 & con2);
                    data2take   = curr_data(idx2take);
                    
                    % store the output
                    data_out(itrl,ichan,idx2take,itime) = data2take; % trl x chan x freq x time
                end %if ci can be computed
            end % if is uniform
        end % for itime
    end % for ichan
end % for itrl
end

