function plot_induced_responses_sensor
% Produces figures stored in:
% output_figures/plot_induced_responses_sensor/xxx

%% %%%%%%%%%%%%%%%%%%%%%%% USER INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set parameters
sens_type       = 'grad';       % {'grad','mag','eeg'} which sensors (gradiometers, magnetometers, eeg)? 
bl_corr         = 'yes';        % {'yes','no'} perform baseline correction? 
bl_wind         = [-0.5 -0.1];  % [from to] baseline window 
time2plot       = [-0.5 1.5];   % [from to] time to plot ERP/ERF
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% internal variables
my_cond         = {'SON','UN'}; % two complementary conditions
cond_nb         = size(my_cond,2);
sleep_stages    = {'W','N1','N2','N3'};
ss_nb           = size(sleep_stages,2);


%% select roi sensors
data_roi        = load('data_roi_sens.mat');
roi_sens        = data_roi.(sens_type);


%% loop across sleep stages
for iss = 1:ss_nb
    mss             = sleep_stages{iss}; % my sleep stage
    tfr_all         = cell(0);
  
    
    %% load data
    for icon = 1:cond_nb     
        % load tfr data
        curr_con        = my_cond{icon};
        tfr_con         = load([mss,'_',sens_type,'_',curr_con,'_tfr_low']);
               
        % store the data
        tfr_all{icon} 	= tfr_con;
        tfr_con=[]; %clean
    end
    
    
    %% combine conditions
    tfr_app             = ft_appendfreq([],tfr_all{:});
    tfr_app.dimord      = 'rpt_chan_freq_time';
    tfr_all=[]; %clear
    
    
    %% extract oscillatory power
    tfr_app.powspctrm   = abs(tfr_app.fourierspctrm);
    tfr_app             = rmfield(tfr_app,'fourierspctrm');
    
        
    %% combine gradiometers
    if strcmp(sens_type, 'grad')
        cfg            = [];
        cfg.method     = 'sum';
        tfr_cmb        = ft_combineplanar(cfg,tfr_app);
    else
        tfr_cmb        = tfr_app;
    end
    tfr_app=[]; %clear
    
    
    %% baseline correction of single trials
    if strcmp(bl_corr,'yes')
        cfg                 = [];
        cfg.baseline        = bl_wind;
        cfg.baselinetype    = 'absolute'; 
        cfg.parameter       = 'powspctrm';
        tfr_bl              = ft_freqbaseline(cfg,tfr_cmb);
    else
        tfr_bl              = tfr_cmb;
    end
    tfr_cmb=[]; %clear
    
    
    %% average across trials
    cfg                 = [];
    cfg.avgoverrpt      = 'yes';
    tfr_avg             = ft_selectdata(cfg,tfr_bl);
    ntrl                = size(tfr_bl.trialinfo,1);
    tfr_bl=[]; %clear
    
    
    %% select shorter time windows
    cfg                 = [];
    cfg.latency         = time2plot;
    tfr_cut             = ft_selectdata(cfg,tfr_avg);
    tfr_avg=[]; %clear
    
    
    %% select ROI
    cfg                 = [];
    cfg.channel         = roi_sens.cmb;
    tfr_cut_roi         = ft_selectdata(cfg,tfr_cut);
    
    
    %% plot all sensors
    my_title        = {['SensNb=All SensType=',sens_type],['CondNb=All SleepStage=',mss],[' TrlNb=',num2str(ntrl)]};
    plot_induced(tfr_cut,my_title)
    savefig(['TFR_',sens_type,'_',mss,'_AllSens_AllCond'])
    tfr_cut=[]; %clear
    
    
    %% plot roi sensors
    my_title        = {['SensNb=Roi SensType=',sens_type],['CondNb=All SleepStage=',mss],[' TrlNb=',num2str(ntrl)]};
    plot_induced(tfr_cut_roi,my_title)
    savefig(['TFR_',sens_type,'_',mss,'_Roi_AllCond'])
    tfr_cut_roi=[]; %clear
end



