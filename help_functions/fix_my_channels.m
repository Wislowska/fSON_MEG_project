function data_out = fix_my_channels(data_in,interpolate,sens_type)
%% channel type
switch sens_type
    case 'grad'
        use_sens    = 'megplanar';
        sens_nb     = 204;
    case 'mag'
        use_sens    = 'megmag';
        sens_nb     = 102;
    case 'eeg'
        use_sens    = 'eeg';
        sens_nb     = 16;
    case 'meg'
        use_sens    = 'meg';
        sens_nb     = 306;
end


%% remove elec field
if ~strcmp(sens_type,'eeg')
    data_in     = rmfield(data_in, 'elec');
end

    
%% select and interpolate channels 
flag = 1;
while(flag)
    %% fix missing channels (for sensor analysis only)
    if interpolate && ~strcmp(sens_type,'eeg')
        data_in.hdr.label   = data_in.grad.label;
        cfg                 = [];
        cfg.senstype        = 'meg';
        cfg.interp          = 'weighted';
        cfg.neigh_method    = 'template';
        data_temp           = obob_fixchannels(cfg, data_in);
        data_fixed          = rmfield(data_temp, 'origLabel'); 
        data_temp=[];
    else
        data_fixed          = data_in;
    end
    
    
    %% select only one type of channels
    cfg             = [];
    cfg.channel     = ft_channelselection(use_sens,data_fixed.label);
    data_mychan     = ft_selectdata(cfg,data_fixed);
    
    
    %% set eeg structure
    if strcmp(sens_type,'eeg')
        data_eeg         = prepare_eeg_layout(data_mychan);
        
        % load neighbours
        neigh           = load('template_neigh.mat');
        neigh           = neigh.my_neigh;
        my_elec         = load('template_elec.mat');
        
        % repair channels
        cfg                 = [];
        cfg.senstype        = 'eeg';
        cfg.method          = 'weighted';
        cfg.neighbours      = neigh;
        cfg.missingchannel  = setdiff(my_elec.label,data_eeg.label);
        data_mychan         = ft_channelrepair(cfg, data_eeg);
        data_fixed         	= data_mychan;
    end
    
    
    %% check number of channels
    if length(data_mychan.label)==sens_nb;flag=0; % quite the loop
    else; data_in = data_fixed; end
    
    
    %% in not interpolating, then quit the loop
    if ~interpolate; flag=0; end
end % while

data_out       = data_mychan;
end