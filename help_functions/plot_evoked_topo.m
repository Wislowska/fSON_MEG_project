function plot_evoked_topo(data_det,my_lay,my_title,sens_type,time2plot_topo)
%% decide on yaxis label
switch sens_type
    case 'grad'; ylab = 'ERF [T]';
    case 'mag'; ylab = 'ERF [T]';
    case 'eeg'; ylab = 'ERP [V]';
end


%% plot
cfg                     = [];
cfg.layout              = my_lay;
cfg.xlim                = time2plot_topo;
cfg.comment             = 'no';
fh_=figure;ft_topoplotER(cfg,data_det); colorbar;


%% make pretty
set(fh_, 'Color', 'w')
d = jet(256);
cmap = colormap(d(256/2:end,:));
ca_ = gca;
ca_.Title.String = my_title;
ca_.Title.FontSize = 12;
fh_.Children(2).Label.String = ylab;
fh_.Children(2).Label.FontSize = 12;


end