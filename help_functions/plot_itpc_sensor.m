function plot_itpc_sensor
% Produces figures stored in:
% output_figures/plot_itpc_sensor/xxx

%% %%%%%%%%%%%%%%%%%%%%%%% USER INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set parameters
sens_type       = 'grad';       % {'grad','mag','eeg'} which sensors (gradiometers, magnetometers, eeg)? 
time2plot       = [-0.5 1.5];   % [from to] time to plot ERP/ERF
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% internal variables
sleep_stages    = {'W','N1','N2','N3'};
ss_nb           = size(sleep_stages,2);
my_cond         = {'SON','UN'}; % two complementary conditions
cond_nb         = size(my_cond,2);


%% select roi sensors
data_roi        = load('data_roi_sens.mat');
roi_sens        = data_roi.(sens_type);


%% load data
for iss = 1:ss_nb
    mss             = sleep_stages{iss}; % my sleep stage
    tfr_all         = cell(0);
    
    %% load data
    for icon = 1:cond_nb
        % load tfr data
        curr_con        = my_cond{icon};
        tfr_con         = load([mss,'_',sens_type,'_',curr_con,'_tfr_low']);
        
        % store the data
        tfr_all{icon} 	= tfr_con;
        tfr_con=[]; %clean
    end
    
    
    %% combine conditions
    tfr_app             = ft_appendfreq([],tfr_all{:});
    tfr_app.dimord      = 'rpt_chan_freq_time';
    tfr_all=[]; %clear
    
    
    %% select shorter data
    cfg             = [];
    cfg.latency     = time2plot;
    data_sel        = ft_selectdata(cfg,tfr_app);
    tfr_app=[];
    
    
    %% extract phase info
    data_sel.fourierspctrm = angle(data_sel.fourierspctrm);
    ntrl            = size(data_sel.fourierspctrm,1);

    
    %% calculate ITPC - low frequencies
    data_size       = size(data_sel.fourierspctrm);
    chan_nb         = size(data_sel.label,1);
    freq_nb         = size(data_sel.freq,2);
    data_out        = nan(data_size(2:4));

    for ichan=1:chan_nb
        for ifreq=1:freq_nb 
            curr_data  = transpose(squeeze(data_sel.fourierspctrm(:,ichan,ifreq,:))); % time x trial
            data_out(ichan,ifreq,:) = abs(mean(exp(1i*curr_data),2)); % compute ITPC
        end
    end
    curr_data=[];
    
    
    %% combine gradiometers
    cfg                 = [];
    cfg.avgoverrpt      = 'yes';
    data_dummy          = ft_selectdata(cfg,data_sel);
    data_dummy.powspctrm   = data_out;
    data_dummy          = rmfield(data_dummy,'fourierspctrm');
    
    if strcmp(sens_type, 'grad') % gradiometeres
        cfg         = [];
        cfg.method  = 'sum';
        meg_cmb  	= ft_combineplanar(cfg,data_dummy);
        meg_cmb.powspctrm = meg_cmb.powspctrm / 2;
    else % magnetometers
        meg_cmb     = data_dummy;
    end
    data_dummy=[];data_out=[];data_sel=[]; %clear
    
    
    %% find ROI channels
    [~,chan2take]   = intersect(meg_cmb.label,roi_sens.cmb);
    
    
    %% average across channels
    data_avg        = squeeze(nanmean(meg_cmb.powspctrm,1)); % freq x time
    data_avg_roi    = squeeze(nanmean(meg_cmb.powspctrm(chan2take,:,:),1)); % freq x time
    
    
    %% interpolate data for plotting
    [data_int,tim_int,fre_int] = interpolate_data(data_avg,meg_cmb.time,meg_cmb.freq);
    [data_int_roi] = interpolate_data(data_avg_roi,meg_cmb.time,meg_cmb.freq);
    
    
    %% plot all sensors
    fh_ = figure(); clf
    imagesc(tim_int,fre_int,data_int)
    axis xy
    xlabel('Time around stimulus onset [sec.]')
    ylabel('Frequency [Hz.]')
    title({['SensNb=All SensType=',sens_type],['SleepStage=',mss],[' TrlNb=',num2str(ntrl)]});
    hcb=colorbar;
    hcb.Label.String  = 'ITPC';
    line([0 0],[fre_int(1) fre_int(end)], 'Color', [.1 .1 .1], 'LineStyle','--', 'LineWidth', 2)
    d = jet(256);
    colormap(d(256/2:end,:));
    set(gca,'FontSize',14);
    set(gcf, 'Color', 'w') 
    myylim      = get(gca,'YLim');
    ylevel      = myylim(2)-myylim(2)/10;
    line([0 0.725],[ylevel ylevel], 'Color', [.3 .3 .3], 'LineStyle','-', 'LineWidth', 2, 'Marker','.','MarkerSize',14) %
    text(0,ylevel,'  avg. stim. duration','Color',[.3 .3 .3],'FontAngle','italic','HorizontalAlignment','left','VerticalAlignment','bottom','FontSize',11)
    savefig(fh_,['ITPC_',sens_type,'_',mss,'_AllSens'])


    %% plot ROI
    fh_ = figure(); clf
    imagesc(tim_int,fre_int,data_int_roi)
    axis xy
    xlabel('Time around stimulus onset [sec.]')
    ylabel('Frequency [Hz.]')
    title({['SensNb=Roi SensType=',sens_type],['SleepStage=',mss],[' TrlNb=',num2str(ntrl)]});
    hcb=colorbar;
    hcb.Label.String  = 'ITPC';
    line([0 0],[fre_int(1) fre_int(end)], 'Color', [.1 .1 .1], 'LineStyle','--', 'LineWidth', 2)
    d = jet(256);
    colormap(d(256/2:end,:));
    set(gca,'FontSize',14);
    set(gcf, 'Color', 'w')
    myylim      = get(gca,'YLim');
    ylevel      = myylim(2)-myylim(2)/10;
    line([0 0.725],[ylevel ylevel], 'Color', [.3 .3 .3], 'LineStyle','-', 'LineWidth', 2, 'Marker','.','MarkerSize',14) %
    text(0,ylevel,'  avg. stim. duration','Color',[.3 .3 .3],'FontAngle','italic','HorizontalAlignment','left','VerticalAlignment','bottom','FontSize',11)
    savefig(fh_,['ITPC_',sens_type,'_',mss,'_Roi'])
    
    data_avg=[];data_avg_roi=[]; %clear
end
end