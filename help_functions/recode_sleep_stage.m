function ss_nb = recode_sleep_stage(sleep_stage)

switch sleep_stage
    case 'W'; ss_nb = 0;
    case 'N1'; ss_nb = 1;
    case 'N2'; ss_nb = 2;
    case 'N3'; ss_nb = 3;
end
end