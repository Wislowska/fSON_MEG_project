function plot_stat_tfr_sensor_prepost
% Produces figures stored in:
% output_figures/plot_stat_tfr_sensor_prepost/xxx

%% %%%%%%%%%%%%%%%%%%%%%%% USER INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set parameters
sens_type       = 'grad';       % {'grad','mag','eeg'} which sensors (gradiometers, magnetometers, eeg)? 
time_pre        = [-0.5 -0.1];  % [from to] time window for the "pre-stimulus" condition
time_post       = [0 1.5];      % [from to] time window for the "post-stimulus" condition
alpha_thres     = 0.05;         % alpha threshold for plotting a cluster at the end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% internal variables
my_cond         = {'SON','UN'}; % two complementary conditions
cond_nb         = size(my_cond,2);
sleep_stages    = {'W','N1','N2','N3'};
ss_nb           = size(sleep_stages,2);


%% select sensor layout template
switch sens_type
    case 'grad'; my_lay = 'neuromag306cmb.lay';
    case 'mag';  my_lay = 'neuromag306mag.lay'; 
    case 'eeg';  my_lay = 'easycapM11';
end


%% loop across sleep stages
for iss = 1:ss_nb
    mss             = sleep_stages{iss}; % my sleep stage
    tfr_all         = cell(0);
  
    
    %% load data
    for icon = 1:cond_nb     
        % load tfr data
        curr_con        = my_cond{icon};
        tfr_con         = load([mss,'_',sens_type,'_',curr_con,'_tfr_low']);
               
        % store the data
        tfr_all{icon} 	= tfr_con;
        tfr_con=[]; %clean
    end
    
    
    %% combine conditions
    tfr_app             = ft_appendfreq([],tfr_all{:});
    tfr_app.dimord      = 'rpt_chan_freq_time';
    tfr_all=[]; %clear
    
    
    %% extract oscillatory power
    tfr_app.powspctrm   = abs(tfr_app.fourierspctrm);
    tfr_app             = rmfield(tfr_app,'fourierspctrm');

    
    %% combine gradiometers
    if strcmp(sens_type, 'grad') % gradiometeres
        cfg         = [];
        cfg.method  = 'sum';
        data_cmb  	= ft_combineplanar(cfg,tfr_app);
    else % magnetometers
        data_cmb    = tfr_app;
    end
    tfr_app=[];
    
    
    %% cut out data for stat
    % post-stimulus
    cfg                     = [];
    cfg.latency             = time_post;
    tfr_post                = ft_selectdata(cfg,data_cmb);
    t_points                = size(tfr_post.powspctrm,4);
    
    % pre-stimulus
    cfg                     = [];
    cfg.latency             = time_pre;
    cfg.avgovertime         = 'yes';
    temp_pre                = ft_selectdata(cfg,data_cmb);
    tfr_pre                 = tfr_post;
    tfr_pre.powspctrm       = repmat(temp_pre.powspctrm,1,1,1,t_points);


    %% calculate neighbours for all sensors
    if strcmp(sens_type,'eeg')
        neighbours      = load('template_neigh.mat');
        neighbours      = neighbours.my_neigh;
    else
        cfg                 = [];
        cfg.method          = 'distance';
        cfg.layout          = my_lay;
        cfg.feedback        = 'no';        
        cfg.neighbourdist   = 0.1;
        neighbours          = ft_prepare_neighbours(cfg, temp_pre);
    end
    
    
    %% design
    n_trl   = size(temp_pre.trialinfo,1);
    design = zeros(2,2*n_trl);
    for itrl = 1:n_trl
        design(1,itrl)       = itrl;
        design(1,n_trl+itrl) = itrl;
    end
    design(2,1:n_trl)         = 1;
    design(2,n_trl+1:2*n_trl) = 2;
    
    
    %% redesign trial structure
    final_pre 	= cell(0);
    final_post  = cell(0);
    
    cfg             = [];
    cfg.avgoverrpt  = 'yes';
    avg_pre         = ft_selectdata(cfg,tfr_pre);
    avg_post        = ft_selectdata(cfg,tfr_post);
    
    for itrl = 1:n_trl
        final_pre{itrl}             = avg_pre;
        final_pre{itrl}.powspctrm   = squeeze(tfr_pre.powspctrm(itrl,:,:,:));
        final_post{itrl}            = avg_post;
        final_post{itrl}.powspctrm  = squeeze(tfr_post.powspctrm(itrl,:,:,:));
    end
    
    
    %% permutation test
    cfg                     = [];
    cfg.latency             = time_post;
    cfg.method              = 'montecarlo';
    cfg.statistic           = 'ft_statfun_depsamplesT';
    cfg.correctm            = 'cluster';
    cfg.clusterstatistic    = 'maxsum';
    cfg.minnbchan           = 2;
    cfg.clusteralpha        = 0.05;
    cfg.clustertail         = 0;
    cfg.tail                = 0; %double-sided
    cfg.alpha               = 0.025; %because tail=0
    cfg.numrandomization    = 1000;
    cfg.design              = design;
    cfg.uvar                = 1;
    cfg.ivar                = 2;  
    cfg.neighbours          = neighbours;
    stat                    = ft_freqstatistics(cfg,final_pre{:},final_post{:});
    

    %% plot the first positive cluster if available and significant
    if ~isempty(stat.posclusters)
        p_clus_pos = stat.posclusters(1).prob;
        if p_clus_pos <= alpha_thres
            plot_cluster(stat,'pos',data_cmb,time_pre,time_post,mss)
            savefig(['stat_',sens_type,'_',mss,'_pos'])
        end
    end
    
    
    %% plot the first negative cluster if available and significant
    if ~isempty(stat.negclusters)
        p_clus_neg = stat.negclusters(1).prob;
        if p_clus_neg <= alpha_thres
            plot_cluster(stat,'neg',data_cmb,time_pre,time_post,mss)
            savefig(['stat_',sens_type,'_',mss,'_neg'])
        end
    end
    
    stat=[];data_cmb=[]; %clean
end

end