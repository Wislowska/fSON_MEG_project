function plot_evoked_responses_sensor
% Produces figures stored in:
% output_figures/plot_evoked_responses_sensor/xxx

%% %%%%%%%%%%%%%%%%%%%%%%% USER INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set parameters
sens_type       = 'grad';       % {'grad','mag','eeg'} which sensors (gradiometers, magnetometers, eeg)? 
bl_corr         = 'yes';        % {'yes','no'} perform baseline correction? 
bl_wind         = [-0.5 -0.1];  % [from to] baseline window 
time2plot       = [-0.5 1.5];   % [from to] time to plot ERP/ERF
time2plot_topo  = [0.1 0.3];    % [from to] time to plot topography of ERP/ERF
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% internal variables
my_cond         = {'FV','SON','UFV','UN'};
cond_nb         = size(my_cond,2);
sleep_stages    = {'W','N1','N2','N3'};
ss_nb           = size(sleep_stages,2);


%% select sensor layout template
switch sens_type
    case 'grad'; my_lay = 'neuromag306cmb.lay'; ev_type = 'ERF';
    case 'mag';  my_lay = 'neuromag306mag.lay'; ev_type = 'ERF';
    case 'eeg';  my_lay = 'easycapM11'; ev_type = 'ERP';
end


%% load data
temp_data       = load('data_preproc.mat');
data_roi        = load('data_roi_sens.mat');
triggers        = load('data_triggers.mat');


%% select roi sensors
roi_sens        = data_roi.(sens_type);


%% loop across sleep stages
for iss = 1:ss_nb
    %% select data for the chosen sleep stage and chosen sensors
    mss             = sleep_stages{iss}; % my sleep stage
    data_sel        = select_sleep_stage(temp_data,mss,sens_type);

    
    %% select pre-selected sub-sample of trials
    trls2take       = load(['TrlInfo_',mss,'.mat']);
    selTrls         = trls2take.trlinfo(:,3);
    currTrls        = data_sel.trialinfo(:,3);
    trls2take       = ismember(currTrls,selTrls);
    
    cfg             = [];
    cfg.trials      = trls2take;
    curr_data       = ft_selectdata(cfg,data_sel);
 
    trl_nb          = size(curr_data.trial,2);
    data_sel=[];%clear
   
    
    %% ---------------------- TLK ACROSS ALL ------------------------------    
    %% time-lock data
    cfg                     = [];
    cfg.preproc.lpfilter    = 'yes';
    cfg.preproc.lpfreq      = 40;
    data_erf                = ft_timelockanalysis(cfg,curr_data);
    
    
    %% baseline correction of averaged data
    if strcmp(bl_corr,'yes')
        cfg                 = [];
        cfg.baseline        = bl_wind;
        cfg.parameter       = 'avg';
        data_erf_bl         = ft_timelockbaseline(cfg,data_erf);
    else
        data_erf_bl         = data_erf;
    end
    data_erf=[];
    
    
    %% combine gradiometers
    if strcmp(sens_type, 'grad')
        cfg                 = [];
        cfg.method          = 'sum';
        data_cmb            = ft_combineplanar(cfg,data_erf_bl);
    else
        data_cmb            = data_erf_bl;
    end
    data_erf_bl=[];
    
    
    %% cut shorter trials
    cfg          	= [];
    cfg.latency   	= time2plot;
    data_cut      	= ft_selectdata(cfg, data_cmb);
    my_time       	= data_cut.time;
    data_cmb=[];
    
    
    %% detrend data
    cfg             = [];
    cfg.detrend     = 'yes';
    data_det        = ft_preprocessing(cfg,data_cut);
    data_cut=[];
    
    
    %% select ROI sensors
    cfg             = [];
    cfg.channel     = roi_sens.cmb;
    data_det_roi    = ft_selectdata(cfg, data_det);
    
    
    %% plot ERF/ERP for all sensors
    data2plot       = squeeze(nanmean(data_det.avg,1));
    my_title        = {['SensNb=all SensType=',sens_type],['CondNb=all SleepStage=',mss],['TrlNb=',num2str(trl_nb)]};
    plot_evoked(data2plot,my_time,my_title,sens_type,bl_wind)
    savefig([ev_type,'_',sens_type,'_',mss,'_AllSens_AllCond'])
        
    % plot topo
    my_title2       = my_title;
    my_title2{3}    = [my_title2{3},[' Toi=[',num2str(time2plot_topo(1)),' ',num2str(time2plot_topo(2)),']']];
    plot_evoked_topo(data_det,my_lay,my_title2,sens_type,time2plot_topo)
    savefig([ev_type,'_',sens_type,'_',mss,'_AllSens_AllCond_topo'])

 
    %% plot ERF/ERP for auditory ROI
    data2plot   = squeeze(nanmean(data_det_roi.avg,1));
    my_title    = {['SensNb=roi SensType=',sens_type],['CondNb=all SleepStage=',mss],[' TrlNb=',num2str(trl_nb)]};
    plot_evoked(data2plot,my_time,my_title,sens_type,bl_wind)
    savefig([ev_type,'_',sens_type,'_',mss,'_Roi_AllCond'])

    
    
    %% ----------------- TLK WITHIN CONDITIONS ---------------------
    %% extract condition trigger
    con_trig        = extract_cond_trig(curr_data.trialinfo(:,1),2);
    data_all_cn     = ones(4,size(my_time,2));
    data_all_cn_roi = ones(4,size(my_time,2));

    
    %% loop through the conditions
    for icon = 1:cond_nb
        curr_con  	= my_cond{icon};
        
        
        %% find triggers
        temp        = find_main_condition(triggers.name,curr_con);
        my_trig     = triggers.nb(~cellfun(@isempty,temp));
        
        
        %% find trials
        cfg             = [];
        cfg.trials      = ismember(con_trig,my_trig);
        data_sel        = ft_selectdata(cfg,curr_data);

                
        %% time-lock data
        cfg                     = [];
        cfg.preproc.lpfilter    = 'yes';
        cfg.preproc.lpfreq      = 40;
        data_erf                = ft_timelockanalysis(cfg,data_sel);
        ntrl                    = size(data_sel.trial,2);
        data_sel=[]; %clear
        
        
        %% baseline correction of averaged data
        if strcmp(bl_corr,'yes')
            cfg                 = [];
            cfg.baseline        = bl_wind;
            cfg.parameter       = 'avg';
            data_erf_bl         = ft_timelockbaseline(cfg,data_erf);
        else
            data_erf_bl         = data_erf;
        end
        data_erf=[]; %clear
        
    
        %% combine gradiometers
        if strcmp(sens_type, 'grad')
            cfg                 = [];
            cfg.method          = 'sum';
            data_cmb            = ft_combineplanar(cfg,data_erf_bl);
        else
            data_cmb            = data_erf_bl;
        end
        data_erf_bl=[];
        
        
        %% cut shorter trials
        cfg            	= [];
        cfg.latency   	= time2plot;
        data_cut       	= ft_selectdata(cfg, data_cmb);
        my_time       	= data_cut.time;
        data_cmb=[];
               
        
        %% detrend data
        cfg             = [];
        cfg.detrend     = 'yes';
        data_det        = ft_preprocessing(cfg,data_cut);
        data_cut=[];
        
        
        %% select top 10 sensors
        cfg             = [];
        cfg.channel     = roi_sens.cmb;
        data_det_roi    = ft_selectdata(cfg, data_det);
        
        
        %% store the data for the average
        data_all_cn(icon,:)     = squeeze(nanmean(data_det.avg,1));
        data_all_cn_roi(icon,:) = squeeze(nanmean(data_det_roi.avg,1));
        data_det=[];data_det_roi=[]; %clear        
    end % icon
    
            
    %% plot ERF for all sensors
    my_title        = {['SensNb=all SensType=',sens_type],['CondNb=4 SleepStage=',mss],[' TrlNb=',num2str(ntrl)]};
    plot_evoked(data_all_cn,my_time,my_title,sens_type,bl_wind,my_cond)
    savefig([ev_type,'_',sens_type,'_',mss,'_AllSens_4Cond'])

    
    %% plot ERF for top10sensors
    my_title    = {['SensNb=roi SensType=',sens_type],['CondNb=4 SleepStage=',mss],[' TrlNb=',num2str(ntrl)]};
    plot_evoked(data_all_cn_roi,my_time,my_title,sens_type,bl_wind,my_cond)
    savefig([ev_type,'_',sens_type,'_',mss,'_Roi_4Cond'])
end
end