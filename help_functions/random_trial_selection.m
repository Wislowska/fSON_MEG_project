function random_trial_selection
% For all following analyses a random selection of trials is used,
%   to assure equal number of trials between conditions and sleep stages.
%   The selected trials are stored in the output folder. In this way, the 
%   same trials are used for each of the following analyses, and can be
%   easily compared with each other.
% The exemplary figures stored on the GIT repository are based on the 
%   random trials that are stored in the \sample_data\trial_info folder.
% You can run the scripts on these pre-selected random trials, or you can
%   draw another set of random trials again, by running this function.
% Please note, that if you run this function, the content of the 
%   \sample_data\trial_info folder will be deleted, and new mat files
%   storing trial information will be created. All of the following
%   analyses will be then conducted using the new selection of trials.
%   Furthermore, your figures will probably slightly differ from the 
%   figures uploaded in the GIT repository.


%% internal variables
my_cond         = {'FV','SON','UFV','UN'};
cond_nb         = size(my_cond,2);
sleep_stages    = {'W','N1','N2','N3'};
ss_nb           = size(sleep_stages,2);


%% delete the old selection of random trials
pt_trls         = what('trial_info');
delete(fullfile(pt_trls.path,'*'))
    

%% load data
temp_data       = load('data_preproc.mat');
triggers        = load('data_triggers.mat');


%% loop across sleep stages
for iss = 1:ss_nb
    %% select data for the chosen sleep stage and chosen sensors
    mss             = sleep_stages{iss}; % my sleep stage
    data_sel        = select_sleep_stage(temp_data,mss,'eeg');
    
    
    %% select random sub-sample of trials and store them on hd for later
    curr_data       = select_random_trials(data_sel,mss);
    con_trig        = extract_cond_trig(curr_data.trialinfo(:,1),2);

    
    
    %% loop through the conditions
    for icon = 1:cond_nb
        curr_con        = my_cond{icon};
        
        
       %% find triggers
        temp            = find_main_condition(triggers.name,curr_con);
        my_trig         = triggers.nb(~cellfun(@isempty,temp));
        
        
        %% find trials
        cfg             = [];
        cfg.trials      = ismember(con_trig,my_trig);
        data_sel        = ft_selectdata(cfg,curr_data);
        
     
        %% save selected trials for later     
        trl_info        = data_sel.trialinfo;
        save(['TrlInfo_',mss,'_',curr_con],'trl_info')
    end
end

end