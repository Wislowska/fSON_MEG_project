function plot_evoked(data2plot,my_time,my_title,sens_type,bl_wind,my_leg)
%% check number of input arguments
if nargin<6; my_leg='';end
con_nb  = size(data2plot,1);


%% decide on yaxis label
switch sens_type
    case 'grad'; ylab = 'ERF [T]';
    case 'mag'; ylab = 'ERF [T]';
    case 'eeg'; ylab = 'ERP [V]';
end


%% plot
fh_ = figure; 
hold on
t=plot(my_time, data2plot, 'LineWidth', 2);


%% make pretty
switch con_nb
    case 1; set(t,'Color', [0 0 0]);
    case 4; set(t,{'color'},{[1 0 0];[0 0 1];[1 0 1];[0 1 1]}) % FV, SON, UFV, UN
end

% add description
set(fh_, 'Color', 'w')
fh_.Children(1).FontSize = 12;
xlim([my_time(1) my_time(end)])
xlabel('Time [sec.]','FontSize',18)
ylabel(ylab,'FontSize',18)

mylim       = ylim;
mylim(2)    = mylim(2)+mylim(2)/10;
ylim(mylim)
line([0 0],mylim, 'Color', [.1 .1 .1], 'LineStyle','--', 'LineWidth', 2)
title(my_title)
ylim(mylim)


%% note baseline and average stimulu duration
ylevel      = mylim(2)-mylim(2)/10;
line(bl_wind,[ylevel ylevel], 'Color', [.3 .3 .3], 'LineStyle','-', 'LineWidth', 2, 'Marker','.','MarkerSize',14) %
text(-0.3,ylevel,'baseline','Color',[.3 .3 .3],'FontAngle','italic','HorizontalAlignment','center','VerticalAlignment','bottom','FontSize',11)
line([0 0.725],[ylevel ylevel], 'Color', [.3 .3 .3], 'LineStyle','-', 'LineWidth', 2, 'Marker','.','MarkerSize',14) %
text(0,ylevel,'  avg. stim. duration','Color',[.3 .3 .3],'FontAngle','italic','HorizontalAlignment','left','VerticalAlignment','bottom','FontSize',11)

hold off
if ~isempty(my_leg);legend(my_leg);end


end