function last_W_trial = find_wake(curr_data,nb_of_w_trl)

trl_nb = curr_data(:,3);

if trl_nb(1) >= nb_of_w_trl % this is in case entire wake file was rejected
    last_W_trial = 0;
else
    found_trl   = 1;
    while found_trl
        last_W_trial    = find(trl_nb == nb_of_w_trl);
        if isempty(last_W_trial)
            nb_of_w_trl = nb_of_w_trl - 1;
        else
            found_trl   = 0;
        end
    end
end
end
