function plot_stat_tfr_sensor_conditions
% Produces figures stored in:
% output_figures/plot_stat_tfr_sensor_conditions/xxx

%% %%%%%%%%%%%%%%%%%%%%%%% USER INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set parameters
sens_type       = 'eeg';       % {'grad','mag','eeg'} which sensors (gradiometers, magnetometers, eeg)? 
time2stat       = [-0.5 1.5];   % [from to] time window for the statistical comparison
alpha_thres     = 0.05;         % alpha threshold for plotting a cluster at the end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% internal variables
contrast1       = {'SON','UN'};
contrast2       = {'FV','UFV'};
cond_nb         = size(contrast1,2);
sleep_stages    = {'W','N1','N2','N3'};
ss_nb           = size(sleep_stages,2);


%% select sensor layout template
switch sens_type
    case 'grad'; my_lay = 'neuromag306cmb.lay';
    case 'mag';  my_lay = 'neuromag306mag.lay'; 
    case 'eeg';  my_lay = 'easycapM11';
end


%% loop across sleep stages
for iss = 1:ss_nb
    mss             = sleep_stages{iss}; % my sleep stage
    tfr_con1        = cell(0); % names
    tfr_con2        = cell(0); % voices
    avg_con1        = cell(0); % names
    avg_con2        = cell(0); % voices
  
    
    %% prepare the data
    for icon = 1:cond_nb    
        %% load the data
        co11            = contrast1{icon};
        co12            = contrast2{icon};
        tfr1            = load([mss,'_',sens_type,'_',co11,'_tfr_low']);
        tfr2            = load([mss,'_',sens_type,'_',co12,'_tfr_low']);
        
        
        %% extract oscillatory power
        tfr1.powspctrm 	= abs(tfr1.fourierspctrm);
        tfr1          	= rmfield(tfr1,'fourierspctrm');
        tfr2.powspctrm 	= abs(tfr2.fourierspctrm);
        tfr2          	= rmfield(tfr2,'fourierspctrm');
        
        
        %% combine gradiometers
        if strcmp(sens_type, 'grad') % gradiometeres
            cfg         = [];
            cfg.method  = 'sum';
            data1_cmb  	= ft_combineplanar(cfg,tfr1);
            data2_cmb  	= ft_combineplanar(cfg,tfr2);
        else % magnetometers
            data1_cmb    = tfr1;
            data2_cmb    = tfr2;
        end
        tfr1=[];tfr2=[];
        
        
        %% cut out time of interest
        cfg             = [];
        cfg.latency    	= time2stat;
        data1_sel     	= ft_selectdata(cfg,data1_cmb);
        data2_sel    	= ft_selectdata(cfg,data2_cmb);
        data1_cmb=[];data2_cmb=[];
        
        
        %% average across trials
        cfg             = [];
        cfg.avgoverrpt  = 'yes';
        avg_con1{icon}  = ft_selectdata(cfg,data1_sel);
        avg_con2{icon}  = ft_selectdata(cfg,data2_sel);
        n_trl           = size(data1_sel.trialinfo,1);
        
       
        %% redesign trial structure
        final1          = cell(0);
        final2          = cell(0);
       
        for itrl = 1:n_trl
            final1{itrl}        	= avg_con1{icon};
            final1{itrl}.powspctrm 	= squeeze(data1_sel.powspctrm(itrl,:,:,:));
            final2{itrl}          	= avg_con1{icon};
            final2{itrl}.powspctrm  = squeeze(data2_sel.powspctrm(itrl,:,:,:));
        end
        data1_sel=[];data2_sel=[]; %clear
        
  
        %% store the data
        tfr_con1{icon}  = final1; % names
        tfr_con2{icon}  = final2; % voices
        final1=[];final2=[];
    end
    
    
    %% calculate power difference for plotting
    cfg             = [];
    cfg.operation   = '(x1-x2) / (x1+x2)';
    cfg.parameter   = 'powspctrm';
    diff_names      = ft_math(cfg,avg_con1{1},avg_con1{2}); % SON - UN
    diff_voices     = ft_math(cfg,avg_con2{1},avg_con2{2}); % SON - UN

    
    %% calculate neighbours for all sensors
    if strcmp(sens_type,'eeg')
        neighbours          = load('template_neigh.mat');
        neighbours          = neighbours.my_neigh;
    else
        cfg                 = [];
        cfg.method          = 'distance';
        cfg.layout          = my_lay;
        cfg.feedback        = 'no';        
        cfg.neighbourdist   = 0.1;
        neighbours          = ft_prepare_neighbours(cfg, diff_names);
    end
    
    
    %% design
    design = zeros(2,2*n_trl);
    for itrl = 1:n_trl
        design(1,itrl)       = itrl;
        design(1,n_trl+itrl) = itrl;
    end
    design(2,1:n_trl)         = 1;
    design(2,n_trl+1:2*n_trl) = 2;
    

    %% permutation test
    cfg                     = [];
    cfg.latency             = time2stat;
    cfg.method              = 'montecarlo';
    cfg.statistic           = 'ft_statfun_depsamplesT';
    cfg.correctm            = 'cluster';
    cfg.clusterstatistic    = 'maxsum';
    cfg.minnbchan           = 2;
    cfg.clusteralpha        = 0.05;
    cfg.clustertail         = 0;
    cfg.tail                = 0; %double-sided
    cfg.alpha               = 0.025; %because tail=0
    cfg.numrandomization    = 1000;
    cfg.design              = design;
    cfg.uvar                = 1;
    cfg.ivar                = 2;  
    cfg.neighbours          = neighbours;
 
    stat_names              = ft_freqstatistics(cfg,tfr_con1{1}{:},tfr_con1{2}{:});
    stat_voices             = ft_freqstatistics(cfg,tfr_con2{1}{:},tfr_con2{2}{:});
    tfr_con1=[];tfr_con2=[]; %clear

    
    %% plot the first positive cluster if available and significant
    % names
    if ~isempty(stat_names.posclusters)
        p_clus_pos = stat_names.posclusters(1).prob;
        if p_clus_pos <= alpha_thres
            plot_cluster_2cond(stat_names,'pos',diff_names,mss,'OWN - OTHER NAME')
            savefig(['stat_',sens_type,'_names_',mss,'_pos'])
        end
    end
    
    % voices
    if ~isempty(stat_voices.posclusters)
        p_clus_pos = stat_voices.posclusters(1).prob;
        if p_clus_pos <= alpha_thres
            plot_cluster_2cond(stat_voices,'pos',diff_voices,mss,'FAMILIAR - UNFAMILIAR VOICE')
            savefig(['stat_',sens_type,'_voices_',mss,'_pos'])
        end
    end
    
    
    %% plot the first negative cluster if available and significant
    % names
    if ~isempty(stat_names.negclusters)
        p_clus_neg = stat_names.negclusters(1).prob;
        if p_clus_neg <= alpha_thres
            plot_cluster_2cond(stat_names,'neg',diff_names,mss,'OWN - OTHER NAME')
            savefig(['stat_',sens_type,'_names_',mss,'_neg'])
        end
    end
    
    % voices
    if ~isempty(stat_voices.negclusters)
        p_clus_neg = stat_voices.negclusters(1).prob;
        if p_clus_neg <= alpha_thres
            plot_cluster_2cond(stat_voices,'neg',diff_voices,mss,'FAMILIAR - UNFAMILIAR VOICE')
            savefig(['stat_',sens_type,'_voices_',mss,'_neg'])
        end
    end
    
    stat_names=[];stat_voices=[];diff_names=[];diff_voices=[]; %clean
end

end