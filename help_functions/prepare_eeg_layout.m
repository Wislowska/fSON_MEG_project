function eeg_out = prepare_eeg_layout(eeg_in)
%% my eeg channels 
my_eeg      = {'Fp1', 'Fpz', 'Fp2', 'F3', 'Fz', 'F4', 'FC5', 'FC6',... 
               'C3', 'Cz', 'C4', 'P3', 'Pz', 'P4', 'O1', 'O2'};
my_eeg_nb   = [1, 20, 2, 4, 5, 6, 21, 24, 9, 10, 11, 14, 15, 16, 18, 19];
eog_chan    = {'EEG035','EEG036','EEG037','EEG038','EEG033','EEG034'};
elec2rm     = length(eog_chan);


%% find EOG channels
eog_nb      = zeros(1,elec2rm);
idx         = 1;
for ichan = 1:elec2rm
    curr_eog     	= strcmp(eeg_in.label,eog_chan{ichan});
    take_eog        = find(curr_eog);
    if ~isempty(take_eog) 
        eog_nb(idx) = take_eog; 
        idx         = idx+1;
    else eog_nb(idx) = []; 
    end
end


%% electrodes to remove
rm_elec     = cell(1,elec2rm+1);
rm_elec{1}  = 'all';
for iel = 1:elec2rm
    rm_elec{iel+1} = ['-',eog_chan{iel}];
end


%% remove gradiometers
eeg_elec        = rmfield(eeg_in,'grad');
hdr             = rmfield(eeg_elec.hdr,'grad');
eeg_elec.hdr    = hdr;


%% remove EOG channels
cfg         = [];
cfg.channel = ft_channelselection(rm_elec,eeg_elec.label);
eeg_sel     = ft_selectdata(cfg,eeg_elec);
eeg_sel.elec.chanpos(eog_nb,:)  = [];
eeg_sel.elec.elecpos(eog_nb,:)  = [];
eeg_sel.elec.chantype(eog_nb,:) = [];
eeg_sel.elec.chanunit(eog_nb,:) = [];
eeg_sel.elec.label(eog_nb,:)    = [];
eeg_sel.hdr.label               = eeg_sel.label;
eeg_sel.hdr.nChans              = length(eeg_sel.label);
eeg_sel.hdr.elec                = eeg_sel.elec;


%% change names of electrodes
eeg_out     = eeg_sel;
for ielec = 1:length(eeg_out.label)
    curr_channel    = eeg_out.label{ielec};
    temp            = strsplit(curr_channel,'G');
    curr_nb         = str2num(temp{2});
    curr_name       = my_eeg( my_eeg_nb == curr_nb );
    
    % rename
    eeg_out.label(ielec)            = curr_name;
    eeg_out.elec.label(ielec)       = curr_name;    
    eeg_out.hdr.label(ielec)        = curr_name;    
    eeg_out.hdr.elec.label(ielec)	= curr_name;  
    eeg_out.cfg.channel(ielec)      = curr_name;
end


%% fix channel positions
% load a template
temp_sens = ft_read_sens('standard_1020.elc');

% go through the electrodes
for ielec = 1:length(eeg_out.label)
    % find position of the current electrode
    curr_elec   = eeg_out.label{ielec};
    temp_nb     = find(strcmp(temp_sens.label,curr_elec));
    curr_pos    = temp_sens.elecpos(temp_nb,:);
    
    % insert it in the output structure
    eeg_out.elec.chanpos(ielec,:) = curr_pos;
    eeg_out.elec.elecpos(ielec,:) = curr_pos;
    eeg_out.hdr.elec.chanpos(ielec,:) = curr_pos;
    eeg_out.hdr.elec.elecpos(ielec,:) = curr_pos;
end
end