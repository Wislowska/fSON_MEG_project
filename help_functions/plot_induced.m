function plot_induced(data_in,my_title)
%% average across channels
cfg                 = [];
cfg.avgoverchan     = 'yes';
data_avg            = ft_selectdata(cfg,data_in);
data_in=[]; %clear


%% interpolate data
[data_int,tim_int,fre_int,my_lim] = interpolate_data(data_avg.powspctrm,data_avg.time,data_avg.freq);


%% plot data
figure;imagesc(tim_int,fre_int,data_int,[-my_lim my_lim]);
mylim     = ylim;
axis xy
colormap('jet')
hcb=colorbar;


%% add description
xlabel('Time around stimulus onset [sec.]')
ylabel('Frequency [Hz.]')
hcb.Label.String  = 'Relative Change in Oscillatory Power';


%% make pretty
cf1         =gca;
cf1.FontSize=14;
set(gcf, 'Color', 'w')
line([0 0],mylim, 'Color', [.1 .1 .1], 'LineStyle','--', 'LineWidth', 2) 
title(my_title)
myylim      = cf1.YLim;
ylevel      = myylim(2)-myylim(2)/10;
line([0 0.725],[ylevel ylevel], 'Color', [.3 .3 .3], 'LineStyle','-', 'LineWidth', 2, 'Marker','.','MarkerSize',14) %
text(0,ylevel,'  avg. stim. duration','Color',[.3 .3 .3],'FontAngle','italic','HorizontalAlignment','left','VerticalAlignment','bottom','FontSize',11)


end