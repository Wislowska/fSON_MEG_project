function plot_filtered_erps_sensor
% Produces figures stored in:
% output_figures/plot_evoked_responses_sensor/xxx


%% %%%%%%%%%%%%%%%%%%%%%%% USER INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set parameters
bl_corr         = 'yes';        % {'yes','no'} perform baseline correction? 
bl_wind         = [-0.5 -0.1];  % [from to] baseline window 
time2plot       = [-0.5 1.5];   % [from to] time to plot ERP/ERF
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% internal variables
sleep_stages    = {'W','N1','N2','N3'};
ss_nb           = size(sleep_stages,2);
freq_boarders   = [1 2; 4 7; 8 12; 11 15; 8 10; 11 13; 10 12; 13 15];
band_nb         = size(freq_boarders,1);


%% load data
temp_data       = load('data_preproc.mat');
data_roi        = load('data_roi_sens.mat');
roi_sens        = data_roi.eeg;


%% loop across sleep stages
for iss = 1:ss_nb
    %% select data for the chosen sleep stage and chosen sensors
    mss             = sleep_stages{iss}; % my sleep stage
    data_sel        = select_sleep_stage(temp_data,mss,'eeg');
    
    
    %% select pre-selected sub-sample of trials
    trls2take       = load(['TrlInfo_',mss,'.mat']);
    selTrls         = trls2take.trlinfo(:,3);
    currTrls        = data_sel.trialinfo(:,3);
    trls2take       = ismember(currTrls,selTrls);
    
    cfg             = [];
    cfg.trials      = trls2take;
    curr_data       = ft_selectdata(cfg,data_sel);
    data_sel=[];%clear
    
    
    %% time-lock data
    cfg                     = [];
    cfg.preproc.lpfilter    = 'yes';
    cfg.preproc.lpfreq      = 40;
    data_erp                = ft_timelockanalysis(cfg,curr_data);
    curr_data=[]; %clear
    
    
    %% baseline correction of averaged data
    if strcmp(bl_corr,'yes')
        cfg                 = [];
        cfg.baseline        = bl_wind;
        cfg.parameter       = 'avg';
        data_bl             = ft_timelockbaseline(cfg,data_erp);
    else
        data_bl             = data_erp;
    end
    data_erp=[];

    
    %% cut shorter trials
    cfg          	= [];
    cfg.latency   	= time2plot;
    data_cut      	= ft_selectdata(cfg, data_bl);
    my_time       	= data_cut.time;
    data_bl=[];
    
    
    %% detrend data
    cfg             = [];
    cfg.detrend     = 'yes';
    data_det        = ft_preprocessing(cfg,data_cut);
    data_cut=[];
    
    
    %% select ROI sensors
    cfg             = [];
    cfg.channel     = roi_sens.cmb;
    data_det_roi    = ft_selectdata(cfg, data_det);
    
    
    %% average across sensors
    cfg             = [];
    cfg.avgoverchan = 'yes';
    data_avg        = ft_selectdata(cfg,data_det);
    data_avg_roi    = ft_selectdata(cfg,data_det_roi);
    data_det=[];data_det_roi=[]; %clear

  
    %% bandpass filter
    tp              = size(data_avg.avg,2);
    filt_sig        = zeros(band_nb,tp);
    filt_sig_roi    = zeros(band_nb,tp);
    
    for ib = 1:band_nb
        cfg                 = [];
        cfg.bpfilter        = 'yes';
        cfg.bpfreq          = freq_boarders(ib,:);
        cfg.bpfilttype      = 'firws';
        fs                  = ft_preprocessing(cfg,data_avg); % Butterworth IIR filter
        fs_roi              = ft_preprocessing(cfg,data_avg_roi); % Butterworth IIR filter
        filt_sig(ib,:)      = fs.avg;      
        filt_sig_roi(ib,:)  = fs_roi.avg;      
    end
    
    
    %% find peaks
    save_name       = ['filtERP_',mss,'_'];
    find_erp_peaks(data_avg,freq_boarders,filt_sig,save_name)

    save_name       = ['filtERP_ROI_',mss,'_'];
    find_erp_peaks(data_avg_roi,freq_boarders,filt_sig_roi,save_name)
    
    
    %% plot filtered signal
    my_time         = data_avg.time;
    save_name       = ['filtERP_',mss,'_ALL'];
    plot_filtered_signal(filt_sig,data_avg.avg,freq_boarders,my_time,save_name)

    save_name       = ['filtERP_ROI_',mss,'_ALL'];
    plot_filtered_signal(filt_sig_roi,data_avg_roi.avg,freq_boarders,my_time,save_name)
    
    data_avg=[];data_avg_roi=[];filt_sig=[];filt_sig_roi=[]; %clear
end
end