function data_out = select_random_trials(data_in,mss)
%% find the minimum number of trials per sleep stage & condition
trl_nb          = importdata('event_number_specific.txt',' ');
triggers        = load('data_triggers.mat');
my_cond         = {'SON_FV','SON_UFV','UN_FV','UN_UFV'};
cond_nb         = size(my_cond,2);

all_trl_nb      = strsplit(trl_nb{2},'\t');
min_trl         = min(str2double(all_trl_nb));


%% select only trials without arousals
hori_data       = importdata('hori_trls.mat');
trial_data      = data_in.trialinfo(:,3);
trl2take        = ~ismember(trial_data,hori_data);

% select trials
cfg             = [];
cfg.trials      = trl2take;
data_arousal    = ft_selectdata(cfg,data_in);
data_in=[]; %clear


%% extract condition triggers
trl_structure   = data_arousal.trialinfo;
con_trig        = extract_cond_trig(trl_structure(:,1),2);


%% pre-allocate output structure
final_trials    = [];


%% loop through the conditions
for icon = 1:cond_nb
    curr_con        = my_cond{icon};    
    
    %% find triggers
    con2take        = contains(triggers.name,curr_con);
    my_trig         = triggers.nb(con2take);
    
    
    %% find trials
    trls2take       = ismember(con_trig,my_trig);
    trls_cond       = trl_structure(trls2take,:);
    
    
    %% select random subset of trials
    trl2take        = randperm(length(trls_cond),min_trl);
    trls_cond_rnd   = trls_cond(trl2take',:);
    
    %% store selected trials
    final_trials    = cat(1,final_trials,trls_cond_rnd);   
end

%% select data
selTrls         = sort(final_trials(:,3));
origTrls        = data_arousal.trialinfo(:,3);

final2take      = ismember(origTrls,selTrls);

cfg             = [];
cfg.trials      = final2take;
data_out        = ft_selectdata(cfg,data_arousal);


%% store the selected trials for later
trlinfo         = data_out.trialinfo;
save(['TrlInfo_',mss],'trlinfo')
end