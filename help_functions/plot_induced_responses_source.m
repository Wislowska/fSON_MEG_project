function plot_induced_responses_source
% Produces figures stored in:
% output_figures/plot_induced_responses_source

%% %%%%%%%%%%%%%%%%%%%%%%% USER INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set parameters
bl_wind         = [-0.5 -0.1];  % [from to] baseline window 
my_toi          = [0.1 0.3];    % [from to] time to plot
my_foi          = [2 5];        % [from to] frequency to plot
plot_thres      = 0.5;          % threshold for masking plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% internal variables
sleep_stages    = {'W','N1','N2','N3'};
ss_nb           = size(sleep_stages,2);


%% load data
% load template grid
temp_grid    	= load('mni_grid_1_5_cm_889pnts');
temp_grid     	= temp_grid.template_grid;
temp_grid      	= ft_convert_units(temp_grid, 'm');

% load template mri
temp_mri       	= ft_read_mri('single_subj_T1.nii');
temp_mri        = ft_convert_units(temp_mri,'m');
temp_mri        = ft_volumereslice([],temp_mri);
temp_seg_mri    = load('template_seg_mri');


%% loop across sleep stages
for iss=1:ss_nb
    mss                 = sleep_stages{iss};
    curr_data           = load(['tfr_source_',mss]);
    
    
    %% put into source space (template brain)
    cfg                 = [];
    cfg.sourcegrid      = temp_grid;
    cfg.parameter       = 'powspctrm';
    cfg.mri             = temp_mri;
    cfg.frequency       = my_foi;
    cfg.latency         = bl_wind;
    source_pre          = obob_svs_virtualsens2source(cfg, curr_data);
    cfg.latency         = my_toi;
    source_post         = obob_svs_virtualsens2source(cfg, curr_data);
    curr_data=[]; %clear

    
    %% calculate pre to post difference
    source_all         	 = source_post;
    source_all.powspctrm = (source_post.powspctrm - source_pre.powspctrm) ./ (source_pre.powspctrm + source_post.powspctrm);
    source_pre=[];source_post=[]; %clear
    
    
    %% threshold
    mymax             	= max(source_all.powspctrm(:));    
    source_all.mask    	= source_all.powspctrm >= ( mymax * plot_thres);
    
    
    %% mask out everything else than brain
    source_all.mask    	= source_all.mask .* temp_seg_mri.brain;
    
    
    %% plot ortho
    cfg                 = [];
    cfg.method          = 'ortho';
    cfg.funparameter    = 'powspctrm';
    cfg.funcolormap     = 'parula';
    cfg.maskparameter   = 'mask';
    cfg.interactive     = 'yes';
    cfg.funcolorlim     = 'zeromax';
    cfg.crosshair       = 'no';
    ft_sourceplot(cfg, source_all);
    fh_ = gcf;
    d = jet(256);
    colormap(d(256/2:end,:));
    
    
    %% save plot
    f1          = num2str(my_foi(1));
    f2          = num2str(my_foi(2));
    t1          = num2str(round(my_toi(1)*1000)); % in ms
    t2          = num2str(round(my_toi(2)*1000)); % in ms
    thr         = num2str(plot_thres*100);

    savefig(fh_,['TFR_',mss,'_Foi-',f1,'-',f2,'Hz_Toi-',t1,'-',t2,'ms_Thr-',thr,'proc'])
    source_all=[]; %clear  
end